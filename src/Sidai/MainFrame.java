/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sidai;

import com.placeholder.PlaceHolder;
import com.toedter.calendar.JDateChooser;
import com.toedter.components.JSpinField;
import java.awt.Color;
import java.awt.Component;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import static javax.swing.JOptionPane.WARNING_MESSAGE;
import static javax.swing.JOptionPane.YES_NO_OPTION;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Ruth
 */
public final class MainFrame extends javax.swing.JFrame {
    Login login = new Login();
    FillTable table = new FillTable();
    Operations op = new Operations();
    FillTable1 table1 = new FillTable1();
    Date date = new Date();
    DBConnection connection = new DBConnection();
    PlaceHolder holder;
    DefaultTableModel model;
    static int saleTotal;
    int total = 0;
    
    // The sql statements used in the tables
    String contacts = "SELECT * FROM contact WHERE name LIKE ?";
    String orders = "SELECT orderid, item, quantity, description FROM orders";
    String stock = "SELECT stockid, item, unit, qty, price, lowCount FROM stock WHERE item LIKE ?";
    String lowQty = "SELECT stockid, item, unit, qty FROM stock WHERE qty <= lowCount";
    String user = "SELECT user_id, userName, userType FROM user WHERE userType != 'Super' AND userName LIKE ?";
    String delivery = "SELECT * FROM delivery";
    String itemSearch = "SELECT stockid, item, unit FROM stock WHERE item LIKE ?";

    /**
     * Creates new form MainFrame
     */
    public MainFrame(){
    }
    
    public MainFrame(String user) {
        initComponents();
        // Maximise the screen
        GraphicsEnvironment e = GraphicsEnvironment.getLocalGraphicsEnvironment();
        this.setMaximizedBounds(e.getMaximumWindowBounds());
        this.setExtendedState(this.getExtendedState() | MainFrame.MAXIMIZED_BOTH);
        
        jLabel7.setText(user);
        saleTableListener();
//        placeholders();
        disableOnStart();
        TPanel(Body, NewSale);
        ChangeColor(Menu, btnSale);
        table.FilterTable(tblSale1, itemSearch, txtSearch2);
    }
    
    // Settting the placeholders
    public void placeholders() {
        holder = new PlaceHolder(txtSearch, "Search by item name");
        holder = new PlaceHolder(txtSearch1, "Search by item name");
        holder = new PlaceHolder(txtSearch2, "Search by item name");
        holder = new PlaceHolder(txtSearch3, "Search by item name");
        holder = new PlaceHolder(jTextField5, "Search by item name");
        holder = new PlaceHolder(txtItem2, "Click the search button");
        holder = new PlaceHolder(txtItem, "Select item from the table");
    }
    
    // buttons to disable on startup
    public void disableOnStart() {
        btnVoid2.setEnabled(false);
        btnVoid1.setEnabled(false);
        btnVoid.setEnabled(false);
        btnDel4.setEnabled(false);
        btnDel3.setEnabled(false);
        btnDel.setEnabled(false);
        btnDel2.setEnabled(false);
        btnDel1.setEnabled(false);
        btnUpdate.setEnabled(false);
        btnUpdate1.setEnabled(false);
        btnUpdate2.setEnabled(false);
        btnDel5.setEnabled(false);
    }
    
    public void reEnable() {
        btnSave.setEnabled(true);
        btnSave1.setEnabled(true);
        btnSave2.setEnabled(true);
        btnSave3.setEnabled(true);
        btnSave4.setEnabled(true);
        btnSave5.setEnabled(true);
        btnSave6.setEnabled(true);
        btnSave7.setEnabled(true);
    }
    
     // Change the color of the selected menu item
    public void ChangeColor (JPanel panel, JButton button) {
for (Component C : panel.getComponents())
{    
    if (C instanceof JButton){
        ((JButton) C).setBackground(new Color(18,19,34));
        button.setBackground(new Color(88,88,88));
    }
}
}
    
    // Change color on normal button hover
    public void btnHoverColor (JButton button) {
        button.setBackground(new Color(94, 61, 169));
    }
    
    // Revert to original color of normal button
    public void btnColor (JButton button) {
        button.setBackground(new Color(56,16,149));
    }
    
    // Change color of menu on hover
    public void menuHover (JButton button) {
        button.setBackground(new Color(45, 43, 63));
    }
    
    // Revert to original color of menu
    public void revertMenu (JButton button) {
        button.setBackground(new Color(18,19,34));
    }
    
    // Setting the selected panel
    public void TPanel (JPanel p1, JPanel p) {
        // remove all panels
        p1.removeAll();
        p1.repaint();
        p1.revalidate();
        
        // set the desired panel
        p1.add(p);
        p1.repaint();
        p1.revalidate();
    }
    
    // Clear textfields and textareas
    public void clrFields (JPanel panel) {
for (Component C : panel.getComponents()) {    
    if (C instanceof JTextComponent){
        ((JTextComponent) C).setText(null);
    }
  }
for (Component Comp : panel.getComponents()) {    
    if (Comp instanceof JDateChooser){
        ((JDateChooser) Comp).setDate(date);
    }
  }
for (Component Co : panel.getComponents()) {
    if (Co instanceof JSpinField) {
        ((JSpinField) Co).setValue(0);
    }
}
}
    
    public void clrTable (JTable table) {
        if ( JOptionPane.showConfirmDialog(null, "Do you wish to remove all the items from the table? The process cannot be reversed", null, YES_NO_OPTION, WARNING_MESSAGE) == JOptionPane.YES_OPTION) {
           while(table.getRowCount() > 0)  {
            ((DefaultTableModel) table.getModel()).removeRow(0);
        } 
        }
    }

    /**
     * This method is called from within the constructor to initialise the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        SalePayment = new javax.swing.JDialog();
        jPanel18 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel22 = new javax.swing.JPanel();
        jPanel23 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        Header = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        Menu = new javax.swing.JPanel();
        btnSale = new javax.swing.JButton();
        btnInvoice = new javax.swing.JButton();
        btnPayments = new javax.swing.JButton();
        btnReports = new javax.swing.JButton();
        btnStock = new javax.swing.JButton();
        btnContacts = new javax.swing.JButton();
        btnSettings = new javax.swing.JButton();
        Body = new javax.swing.JPanel();
        NewSale = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        txtSearch2 = new javax.swing.JTextField();
        txtSupplier1 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        btnNew2 = new javax.swing.JButton();
        btnVoid2 = new javax.swing.JButton();
        btnCancel2 = new javax.swing.JButton();
        btnProcess = new javax.swing.JButton();
        btnPending1 = new javax.swing.JButton();
        lblSaleNo = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tblSale = new javax.swing.JTable();
        jScrollPane13 = new javax.swing.JScrollPane();
        tblSale1 = new javax.swing.JTable();
        jLabel9 = new javax.swing.JLabel();
        Invoice = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblInvoice = new javax.swing.JTable();
        jLabel10 = new javax.swing.JLabel();
        txtSearch1 = new javax.swing.JTextField();
        txtCustomer = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtTotal1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnVoid1 = new javax.swing.JButton();
        btnCancel1 = new javax.swing.JButton();
        btnNew = new javax.swing.JButton();
        btnSave1 = new javax.swing.JButton();
        btnPay = new javax.swing.JButton();
        lblSaleNo1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        Payments = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPayment = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        txtSupplier = new javax.swing.JTextField();
        txtTotal2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnVoid = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        btnNew1 = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnPending = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lblSaleNo2 = new javax.swing.JLabel();
        Reports = new javax.swing.JPanel();
        ReportsTab = new javax.swing.JTabbedPane();
        InvoiceReport = new javax.swing.JPanel();
        PaymentReport = new javax.swing.JPanel();
        DeliveryReport = new javax.swing.JPanel();
        SalesReport = new javax.swing.JPanel();
        Inventory = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        StockTab = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tblStock = new javax.swing.JTable();
        jLabel14 = new javax.swing.JLabel();
        txtSearch3 = new javax.swing.JTextField();
        btnStockReport = new javax.swing.JButton();
        LowStock = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        tblLow = new javax.swing.JTable();
        jScrollPane11 = new javax.swing.JScrollPane();
        tblOrder = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        txtItem3 = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtDes = new javax.swing.JTextArea();
        jPanel13 = new javax.swing.JPanel();
        btnCancel5 = new javax.swing.JButton();
        btnSave6 = new javax.swing.JButton();
        btnDel4 = new javax.swing.JButton();
        txtItem4 = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        ManageItems = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        tblManageStock = new javax.swing.JTable();
        jTextField16 = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        btnCancel3 = new javax.swing.JButton();
        btnUpdate2 = new javax.swing.JButton();
        btnDel3 = new javax.swing.JButton();
        jLabel22 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        txtItem = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        txtQty = new com.toedter.components.JSpinField();
        txtPrice = new com.toedter.components.JSpinField();
        txtPrice1 = new com.toedter.components.JSpinField();
        jLabel33 = new javax.swing.JLabel();
        txtUnit1 = new javax.swing.JTextField();
        DeliveryTab = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblDelivery = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        btnClr1 = new javax.swing.JButton();
        btnSave5 = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnDel = new javax.swing.JButton();
        btnSave2 = new javax.swing.JButton();
        jLabel26 = new javax.swing.JLabel();
        txtItem1 = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        txtUnt = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        txtLowCnt = new com.toedter.components.JSpinField();
        txtQty2 = new com.toedter.components.JSpinField();
        txtPrice2 = new com.toedter.components.JSpinField();
        CategoryTab = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jTextField5 = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        btnCancel4 = new javax.swing.JButton();
        btnSave3 = new javax.swing.JButton();
        btnDel1 = new javax.swing.JButton();
        txtSubUnit = new com.toedter.components.JSpinField();
        txtQty1 = new com.toedter.components.JSpinField();
        txtItem2 = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        txtSubUnit1 = new com.toedter.components.JSpinField();
        Contacts = new javax.swing.JPanel();
        jTextField12 = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        tblCustomer = new javax.swing.JTable();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        btnSave4 = new javax.swing.JButton();
        btnUpdate1 = new javax.swing.JButton();
        btnDel2 = new javax.swing.JButton();
        txtPhone = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        Setting = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        btnChngPass = new javax.swing.JButton();
        btnMngUser = new javax.swing.JButton();
        btnBackup = new javax.swing.JButton();
        btnRestore = new javax.swing.JButton();
        btnLogout = new javax.swing.JButton();
        jPanel20 = new javax.swing.JPanel();
        ChangePassword = new javax.swing.JPanel();
        jPanel21 = new javax.swing.JPanel();
        txtcurr = new javax.swing.JPasswordField();
        jLabel21 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        txtnew = new javax.swing.JPasswordField();
        txtconfirm = new javax.swing.JPasswordField();
        jLabel40 = new javax.swing.JLabel();
        btnUpdatePass1 = new javax.swing.JButton();
        jCheckBox5 = new javax.swing.JCheckBox();
        jPanel16 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        MngUsers = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        btnSave7 = new javax.swing.JButton();
        btnDel5 = new javax.swing.JButton();
        jLabel42 = new javax.swing.JLabel();
        txtName1 = new javax.swing.JTextField();
        jLabel44 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jScrollPane12 = new javax.swing.JScrollPane();
        tblCustomer1 = new javax.swing.JTable();
        jTextField13 = new javax.swing.JTextField();
        jLabel43 = new javax.swing.JLabel();

        SalePayment.setMinimumSize(new java.awt.Dimension(690, 460));
        SalePayment.setModal(true);
        SalePayment.setPreferredSize(new java.awt.Dimension(690, 460));
        SalePayment.setResizable(false);

        jPanel18.setBackground(new java.awt.Color(18, 19, 34));
        jPanel18.setPreferredSize(new java.awt.Dimension(654, 80));

        jLabel1.setFont(new java.awt.Font("Calisto MT", 0, 38)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("PAYMENT");

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 666, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
        );

        SalePayment.getContentPane().add(jPanel18, java.awt.BorderLayout.PAGE_START);

        jPanel22.setBackground(new java.awt.Color(255, 255, 255));

        jPanel23.setBackground(new java.awt.Color(255, 255, 255));
        jPanel23.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 25, 5));

        jLabel6.setBackground(new java.awt.Color(255, 255, 255));
        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("KES");
        jPanel23.add(jLabel6);

        jLabel16.setBackground(new java.awt.Color(255, 255, 255));
        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel16.setText("15,000");
        jPanel23.add(jLabel16);

        jLabel46.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel46.setText("cash");

        jLabel47.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel47.setText("M-Pesa");

        jLabel48.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel48.setText("Cheque");

        jLabel49.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel49.setText("Other");

        jButton1.setBackground(new java.awt.Color(56, 16, 149));
        jButton1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("Print");
        jButton1.setContentAreaFilled(false);
        jButton1.setOpaque(true);

        jTextField1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        jTextField2.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        jTextField3.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        jTextField4.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        jButton2.setBackground(new java.awt.Color(200, 0, 0));
        jButton2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setText("Cancel");
        jButton2.setContentAreaFilled(false);
        jButton2.setOpaque(true);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addGap(140, 140, 140)
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel46)
                    .addComponent(jLabel47)
                    .addComponent(jLabel48)
                    .addComponent(jLabel49))
                .addGap(18, 18, 18)
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(139, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel22Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel23, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel46, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel47, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel48, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel49, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        SalePayment.getContentPane().add(jPanel22, java.awt.BorderLayout.CENTER);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(1300, 900));
        setPreferredSize(new java.awt.Dimension(1300, 900));

        Header.setBackground(new java.awt.Color(18, 19, 34));
        Header.setMinimumSize(new java.awt.Dimension(904, 60));
        Header.setPreferredSize(new java.awt.Dimension(904, 60));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/User_25px.png"))); // NOI18N
        jLabel7.setText("Administrator");
        jLabel7.setIconTextGap(5);

        jLabel8.setFont(new java.awt.Font("Calisto MT", 0, 40)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("SIDAI HARDWARE");

        javax.swing.GroupLayout HeaderLayout = new javax.swing.GroupLayout(Header);
        Header.setLayout(HeaderLayout);
        HeaderLayout.setHorizontalGroup(
            HeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderLayout.createSequentialGroup()
                .addGap(253, 253, 253)
                .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, 567, Short.MAX_VALUE)
                .addGap(180, 180, 180)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );
        HeaderLayout.setVerticalGroup(
            HeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderLayout.createSequentialGroup()
                .addGroup(HeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(HeaderLayout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, HeaderLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(3, 3, 3)))
                .addGap(7, 7, 7))
        );

        getContentPane().add(Header, java.awt.BorderLayout.PAGE_START);

        Menu.setBackground(new java.awt.Color(18, 19, 34));
        Menu.setMinimumSize(new java.awt.Dimension(100, 486));
        Menu.setLayout(new java.awt.GridLayout(9, 1, 0, 1));

        btnSale.setBackground(new java.awt.Color(18, 19, 34));
        btnSale.setFont(new java.awt.Font("Tahoma", 0, 22)); // NOI18N
        btnSale.setForeground(new java.awt.Color(255, 255, 255));
        btnSale.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Sell_30px.png"))); // NOI18N
        btnSale.setText("New Sale");
        btnSale.setBorderPainted(false);
        btnSale.setContentAreaFilled(false);
        btnSale.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSale.setOpaque(true);
        btnSale.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaleActionPerformed(evt);
            }
        });
        Menu.add(btnSale);

        btnInvoice.setBackground(new java.awt.Color(18, 19, 34));
        btnInvoice.setFont(new java.awt.Font("Tahoma", 0, 22)); // NOI18N
        btnInvoice.setForeground(new java.awt.Color(255, 255, 255));
        btnInvoice.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Invoice_30px.png"))); // NOI18N
        btnInvoice.setText("Invoice");
        btnInvoice.setBorderPainted(false);
        btnInvoice.setContentAreaFilled(false);
        btnInvoice.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnInvoice.setOpaque(true);
        btnInvoice.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnInvoice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInvoiceActionPerformed(evt);
            }
        });
        Menu.add(btnInvoice);

        btnPayments.setBackground(new java.awt.Color(18, 19, 34));
        btnPayments.setFont(new java.awt.Font("Tahoma", 0, 22)); // NOI18N
        btnPayments.setForeground(new java.awt.Color(255, 255, 255));
        btnPayments.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Bill_30px.png"))); // NOI18N
        btnPayments.setText("Debts");
        btnPayments.setBorderPainted(false);
        btnPayments.setContentAreaFilled(false);
        btnPayments.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPayments.setOpaque(true);
        btnPayments.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPayments.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPaymentsActionPerformed(evt);
            }
        });
        Menu.add(btnPayments);

        btnReports.setBackground(new java.awt.Color(18, 19, 34));
        btnReports.setFont(new java.awt.Font("Tahoma", 0, 22)); // NOI18N
        btnReports.setForeground(new java.awt.Color(255, 255, 255));
        btnReports.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Combo Chart_30px.png"))); // NOI18N
        btnReports.setText("Reports");
        btnReports.setBorderPainted(false);
        btnReports.setContentAreaFilled(false);
        btnReports.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnReports.setOpaque(true);
        btnReports.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnReports.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReportsActionPerformed(evt);
            }
        });
        Menu.add(btnReports);

        btnStock.setBackground(new java.awt.Color(18, 19, 34));
        btnStock.setFont(new java.awt.Font("Tahoma", 0, 22)); // NOI18N
        btnStock.setForeground(new java.awt.Color(255, 255, 255));
        btnStock.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Scan Stock_30px.png"))); // NOI18N
        btnStock.setText("Inventory");
        btnStock.setBorderPainted(false);
        btnStock.setContentAreaFilled(false);
        btnStock.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnStock.setOpaque(true);
        btnStock.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStockActionPerformed(evt);
            }
        });
        Menu.add(btnStock);

        btnContacts.setBackground(new java.awt.Color(18, 19, 34));
        btnContacts.setFont(new java.awt.Font("Tahoma", 0, 22)); // NOI18N
        btnContacts.setForeground(new java.awt.Color(255, 255, 255));
        btnContacts.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Contacts_30px.png"))); // NOI18N
        btnContacts.setText("Contacts");
        btnContacts.setBorderPainted(false);
        btnContacts.setContentAreaFilled(false);
        btnContacts.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnContacts.setOpaque(true);
        btnContacts.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnContacts.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnContactsActionPerformed(evt);
            }
        });
        Menu.add(btnContacts);

        btnSettings.setBackground(new java.awt.Color(18, 19, 34));
        btnSettings.setFont(new java.awt.Font("Tahoma", 0, 22)); // NOI18N
        btnSettings.setForeground(new java.awt.Color(255, 255, 255));
        btnSettings.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/icons8-administrative-tools-30.png"))); // NOI18N
        btnSettings.setText("Settings");
        btnSettings.setBorderPainted(false);
        btnSettings.setContentAreaFilled(false);
        btnSettings.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSettings.setOpaque(true);
        btnSettings.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSettings.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSettingsActionPerformed(evt);
            }
        });
        Menu.add(btnSettings);

        getContentPane().add(Menu, java.awt.BorderLayout.WEST);

        Body.setBackground(new java.awt.Color(255, 255, 255));
        Body.setLayout(new java.awt.CardLayout());

        NewSale.setBackground(new java.awt.Color(255, 255, 255));
        NewSale.setPreferredSize(new java.awt.Dimension(1045, 725));

        jLabel12.setBackground(new java.awt.Color(255, 255, 255));
        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 0, 153));
        jLabel12.setText("M/Ms:");

        txtSearch2.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        txtSearch2.setToolTipText("Search by item name");
        txtSearch2.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtSearch2CaretUpdate(evt);
            }
        });

        txtSupplier1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        txtSupplier1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSupplier1ActionPerformed(evt);
            }
        });

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Search_20px.png"))); // NOI18N

        txtTotal.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        txtTotal.setText("10,000");

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        java.awt.FlowLayout flowLayout1 = new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 10, 10);
        flowLayout1.setAlignOnBaseline(true);
        jPanel3.setLayout(flowLayout1);

        btnNew2.setBackground(new java.awt.Color(56, 16, 149));
        btnNew2.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        btnNew2.setForeground(new java.awt.Color(255, 255, 255));
        btnNew2.setText("New");
        btnNew2.setBorderPainted(false);
        btnNew2.setContentAreaFilled(false);
        btnNew2.setMinimumSize(new java.awt.Dimension(125, 50));
        btnNew2.setOpaque(true);
        btnNew2.setPreferredSize(new java.awt.Dimension(125, 50));
        btnNew2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnNew2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnNew2MouseExited(evt);
            }
        });
        btnNew2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNew2ActionPerformed(evt);
            }
        });
        jPanel3.add(btnNew2);

        btnVoid2.setBackground(new java.awt.Color(56, 16, 149));
        btnVoid2.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        btnVoid2.setForeground(new java.awt.Color(255, 255, 255));
        btnVoid2.setText("Delete");
        btnVoid2.setBorderPainted(false);
        btnVoid2.setContentAreaFilled(false);
        btnVoid2.setMinimumSize(new java.awt.Dimension(125, 50));
        btnVoid2.setOpaque(true);
        btnVoid2.setPreferredSize(new java.awt.Dimension(125, 50));
        btnVoid2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnVoid2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnVoid2MouseExited(evt);
            }
        });
        btnVoid2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoid2ActionPerformed(evt);
            }
        });
        jPanel3.add(btnVoid2);

        btnCancel2.setBackground(new java.awt.Color(220, 0, 0));
        btnCancel2.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        btnCancel2.setForeground(new java.awt.Color(255, 255, 255));
        btnCancel2.setText("Cancel");
        btnCancel2.setBorderPainted(false);
        btnCancel2.setContentAreaFilled(false);
        btnCancel2.setMinimumSize(new java.awt.Dimension(125, 50));
        btnCancel2.setOpaque(true);
        btnCancel2.setPreferredSize(new java.awt.Dimension(125, 50));
        btnCancel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCancel2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCancel2MouseExited(evt);
            }
        });
        btnCancel2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancel2ActionPerformed(evt);
            }
        });
        jPanel3.add(btnCancel2);

        btnProcess.setBackground(new java.awt.Color(56, 16, 149));
        btnProcess.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        btnProcess.setForeground(new java.awt.Color(255, 255, 255));
        btnProcess.setText("Process");
        btnProcess.setBorderPainted(false);
        btnProcess.setContentAreaFilled(false);
        btnProcess.setMinimumSize(new java.awt.Dimension(125, 50));
        btnProcess.setOpaque(true);
        btnProcess.setPreferredSize(new java.awt.Dimension(125, 50));
        btnProcess.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnProcessMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnProcessMouseExited(evt);
            }
        });
        btnProcess.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcessActionPerformed(evt);
            }
        });
        jPanel3.add(btnProcess);

        btnPending1.setBackground(new java.awt.Color(56, 16, 149));
        btnPending1.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        btnPending1.setForeground(new java.awt.Color(255, 255, 255));
        btnPending1.setText("Pending");
        btnPending1.setBorderPainted(false);
        btnPending1.setContentAreaFilled(false);
        btnPending1.setMinimumSize(new java.awt.Dimension(125, 50));
        btnPending1.setOpaque(true);
        btnPending1.setPreferredSize(new java.awt.Dimension(125, 50));
        btnPending1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnPending1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnPending1MouseExited(evt);
            }
        });
        jPanel3.add(btnPending1);

        lblSaleNo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblSaleNo.setText("1000000");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(56, 16, 149));
        jLabel2.setText("Sale No.");

        jScrollPane6.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tblSale.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        tblSale.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Item", "Qty", "Price", "Amount"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblSale.setGridColor(new java.awt.Color(153, 153, 153));
        tblSale.setRowHeight(22);
        tblSale.getTableHeader().setResizingAllowed(false);
        tblSale.getTableHeader().setReorderingAllowed(false);
        tblSale.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblSaleMouseClicked(evt);
            }
        });
        tblSale.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
                tblSaleCaretPositionChanged(evt);
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
            }
        });
        tblSale.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                tblSalePropertyChange(evt);
            }
        });
        jScrollPane6.setViewportView(tblSale);
        if (tblSale.getColumnModel().getColumnCount() > 0) {
            tblSale.getColumnModel().getColumn(0).setResizable(false);
            tblSale.getColumnModel().getColumn(0).setPreferredWidth(100);
            tblSale.getColumnModel().getColumn(1).setResizable(false);
            tblSale.getColumnModel().getColumn(1).setPreferredWidth(600);
            tblSale.getColumnModel().getColumn(2).setResizable(false);
            tblSale.getColumnModel().getColumn(2).setPreferredWidth(150);
            tblSale.getColumnModel().getColumn(3).setResizable(false);
            tblSale.getColumnModel().getColumn(3).setPreferredWidth(150);
            tblSale.getColumnModel().getColumn(4).setResizable(false);
            tblSale.getColumnModel().getColumn(4).setPreferredWidth(200);
        }

        jScrollPane13.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane13.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tblSale1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        tblSale1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Item", "Unit"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblSale1.setGridColor(new java.awt.Color(153, 153, 153));
        tblSale1.setRowHeight(22);
        tblSale1.getTableHeader().setResizingAllowed(false);
        tblSale1.getTableHeader().setReorderingAllowed(false);
        tblSale1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tblSale1FocusLost(evt);
            }
        });
        tblSale1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblSale1MouseClicked(evt);
            }
        });
        jScrollPane13.setViewportView(tblSale1);
        if (tblSale1.getColumnModel().getColumnCount() > 0) {
            tblSale1.getColumnModel().getColumn(0).setResizable(false);
            tblSale1.getColumnModel().getColumn(0).setPreferredWidth(100);
            tblSale1.getColumnModel().getColumn(1).setResizable(false);
            tblSale1.getColumnModel().getColumn(1).setPreferredWidth(600);
            tblSale1.getColumnModel().getColumn(2).setResizable(false);
            tblSale1.getColumnModel().getColumn(2).setPreferredWidth(150);
        }

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel9.setText("Total:");

        javax.swing.GroupLayout NewSaleLayout = new javax.swing.GroupLayout(NewSale);
        NewSale.setLayout(NewSaleLayout);
        NewSaleLayout.setHorizontalGroup(
            NewSaleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(NewSaleLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(NewSaleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(NewSaleLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblSaleNo, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(NewSaleLayout.createSequentialGroup()
                        .addGroup(NewSaleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(NewSaleLayout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtSearch2))
                            .addComponent(jScrollPane13, javax.swing.GroupLayout.DEFAULT_SIZE, 463, Short.MAX_VALUE))
                        .addGap(11, 11, 11)
                        .addGroup(NewSaleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 547, Short.MAX_VALUE)
                            .addGroup(NewSaleLayout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtSupplier1))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NewSaleLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel9)
                                .addGap(18, 18, 18)
                                .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        NewSaleLayout.setVerticalGroup(
            NewSaleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(NewSaleLayout.createSequentialGroup()
                .addGroup(NewSaleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(NewSaleLayout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addGroup(NewSaleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(NewSaleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtSearch2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane13))
                    .addGroup(NewSaleLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(NewSaleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(lblSaleNo))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtSupplier1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(NewSaleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        Body.add(NewSale, "card2");

        Invoice.setBackground(new java.awt.Color(255, 255, 255));
        Invoice.setPreferredSize(new java.awt.Dimension(1045, 725));

        jScrollPane2.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tblInvoice.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        tblInvoice.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"Savanna Cement", "6", "600", "3200"},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Item", "Qty", "Price", "Amount"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblInvoice.setDropMode(javax.swing.DropMode.ON);
        tblInvoice.setGridColor(new java.awt.Color(153, 153, 153));
        tblInvoice.setPreferredSize(new java.awt.Dimension(800, 80));
        tblInvoice.setRowHeight(22);
        tblInvoice.getTableHeader().setResizingAllowed(false);
        tblInvoice.getTableHeader().setReorderingAllowed(false);
        tblInvoice.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblInvoiceMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblInvoice);
        if (tblInvoice.getColumnModel().getColumnCount() > 0) {
            tblInvoice.getColumnModel().getColumn(0).setResizable(false);
            tblInvoice.getColumnModel().getColumn(0).setPreferredWidth(600);
            tblInvoice.getColumnModel().getColumn(1).setResizable(false);
            tblInvoice.getColumnModel().getColumn(1).setPreferredWidth(100);
            tblInvoice.getColumnModel().getColumn(2).setResizable(false);
            tblInvoice.getColumnModel().getColumn(2).setPreferredWidth(120);
            tblInvoice.getColumnModel().getColumn(3).setResizable(false);
            tblInvoice.getColumnModel().getColumn(3).setPreferredWidth(180);
        }

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Search_20px.png"))); // NOI18N

        txtSearch1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        txtSearch1.setToolTipText("");
        txtSearch1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearch1KeyPressed(evt);
            }
        });

        txtCustomer.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        jLabel11.setBackground(new java.awt.Color(255, 255, 255));
        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 0, 153));
        jLabel11.setText("M/Ms:");

        txtTotal1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtTotal1.setText("10,000");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new java.awt.GridLayout(3, 2, 10, 10));

        btnVoid1.setBackground(new java.awt.Color(56, 16, 149));
        btnVoid1.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        btnVoid1.setForeground(new java.awt.Color(255, 255, 255));
        btnVoid1.setText("<html>Delete <br> Item</html>");
        btnVoid1.setBorderPainted(false);
        btnVoid1.setContentAreaFilled(false);
        btnVoid1.setOpaque(true);
        btnVoid1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnVoid1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnVoid1MouseExited(evt);
            }
        });
        btnVoid1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoid1ActionPerformed(evt);
            }
        });
        jPanel1.add(btnVoid1);

        btnCancel1.setBackground(new java.awt.Color(220, 0, 0));
        btnCancel1.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        btnCancel1.setForeground(new java.awt.Color(255, 255, 255));
        btnCancel1.setText("<html>Cancel <br> Invoice</html>");
        btnCancel1.setBorderPainted(false);
        btnCancel1.setContentAreaFilled(false);
        btnCancel1.setOpaque(true);
        btnCancel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCancel1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCancel1MouseExited(evt);
            }
        });
        btnCancel1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancel1ActionPerformed(evt);
            }
        });
        jPanel1.add(btnCancel1);

        btnNew.setBackground(new java.awt.Color(56, 16, 149));
        btnNew.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        btnNew.setForeground(new java.awt.Color(255, 255, 255));
        btnNew.setText("<html>New <br> Invoice</html>");
        btnNew.setBorderPainted(false);
        btnNew.setContentAreaFilled(false);
        btnNew.setOpaque(true);
        btnNew.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnNewMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnNewMouseExited(evt);
            }
        });
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });
        jPanel1.add(btnNew);

        btnSave1.setBackground(new java.awt.Color(56, 16, 149));
        btnSave1.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        btnSave1.setForeground(new java.awt.Color(255, 255, 255));
        btnSave1.setText("Save");
        btnSave1.setBorderPainted(false);
        btnSave1.setContentAreaFilled(false);
        btnSave1.setOpaque(true);
        btnSave1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSave1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSave1MouseExited(evt);
            }
        });
        btnSave1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSave1ActionPerformed(evt);
            }
        });
        jPanel1.add(btnSave1);

        btnPay.setBackground(new java.awt.Color(56, 16, 149));
        btnPay.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        btnPay.setForeground(new java.awt.Color(255, 255, 255));
        btnPay.setText("<html>Customer<br>Payment</html>");
        btnPay.setBorderPainted(false);
        btnPay.setContentAreaFilled(false);
        btnPay.setOpaque(true);
        btnPay.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnPayMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnPayMouseExited(evt);
            }
        });
        btnPay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPayActionPerformed(evt);
            }
        });
        jPanel1.add(btnPay);

        lblSaleNo1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblSaleNo1.setText("1000000");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(56, 16, 149));
        jLabel4.setText("Invoice No.");

        javax.swing.GroupLayout InvoiceLayout = new javax.swing.GroupLayout(Invoice);
        Invoice.setLayout(InvoiceLayout);
        InvoiceLayout.setHorizontalGroup(
            InvoiceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InvoiceLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(InvoiceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InvoiceLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(InvoiceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTotal1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, InvoiceLayout.createSequentialGroup()
                        .addGroup(InvoiceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 779, Short.MAX_VALUE)
                            .addGroup(InvoiceLayout.createSequentialGroup()
                                .addGroup(InvoiceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(InvoiceLayout.createSequentialGroup()
                                        .addGap(21, 21, 21)
                                        .addComponent(jLabel10))
                                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(InvoiceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtSearch1)
                                    .addComponent(txtCustomer))))
                        .addGap(70, 70, 70)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblSaleNo1, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        InvoiceLayout.setVerticalGroup(
            InvoiceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(InvoiceLayout.createSequentialGroup()
                .addGroup(InvoiceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InvoiceLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(InvoiceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSearch1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(InvoiceLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(InvoiceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblSaleNo1)
                            .addComponent(jLabel4))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(InvoiceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(InvoiceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(InvoiceLayout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtTotal1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 629, Short.MAX_VALUE))
                .addGap(11, 11, 11))
        );

        Body.add(Invoice, "card3");

        Payments.setBackground(new java.awt.Color(255, 255, 255));
        Payments.setPreferredSize(new java.awt.Dimension(1045, 725));

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tblPayment.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        tblPayment.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"Savanna Cement", "6", "600", "3200"},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Item", "Qty", "Price", "Amount"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblPayment.setGridColor(new java.awt.Color(153, 153, 153));
        tblPayment.setPreferredSize(new java.awt.Dimension(800, 80));
        tblPayment.setRowHeight(22);
        tblPayment.getTableHeader().setResizingAllowed(false);
        tblPayment.getTableHeader().setReorderingAllowed(false);
        tblPayment.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblPaymentMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblPayment);
        if (tblPayment.getColumnModel().getColumnCount() > 0) {
            tblPayment.getColumnModel().getColumn(0).setResizable(false);
            tblPayment.getColumnModel().getColumn(0).setPreferredWidth(600);
            tblPayment.getColumnModel().getColumn(1).setResizable(false);
            tblPayment.getColumnModel().getColumn(1).setPreferredWidth(100);
            tblPayment.getColumnModel().getColumn(2).setResizable(false);
            tblPayment.getColumnModel().getColumn(2).setPreferredWidth(120);
            tblPayment.getColumnModel().getColumn(3).setResizable(false);
            tblPayment.getColumnModel().getColumn(3).setPreferredWidth(180);
            tblPayment.getColumnModel().getColumn(3).setHeaderValue("Amount");
        }

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 153));
        jLabel3.setText("M/Ms:");

        txtSearch.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        txtSearch.setToolTipText("");

        txtSupplier.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        txtTotal2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtTotal2.setText("10,000");

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new java.awt.GridLayout(3, 2, 10, 10));

        btnVoid.setBackground(new java.awt.Color(56, 16, 149));
        btnVoid.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        btnVoid.setForeground(new java.awt.Color(255, 255, 255));
        btnVoid.setText("<html>Remove<br>Item</html>");
        btnVoid.setToolTipText("");
        btnVoid.setBorderPainted(false);
        btnVoid.setContentAreaFilled(false);
        btnVoid.setOpaque(true);
        btnVoid.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnVoidMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnVoidMouseExited(evt);
            }
        });
        btnVoid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoidActionPerformed(evt);
            }
        });
        jPanel2.add(btnVoid);

        btnCancel.setBackground(new java.awt.Color(230, 34, 20));
        btnCancel.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        btnCancel.setForeground(new java.awt.Color(255, 255, 255));
        btnCancel.setText("<html>Cancel <br>Order</html>");
        btnCancel.setToolTipText("");
        btnCancel.setBorderPainted(false);
        btnCancel.setContentAreaFilled(false);
        btnCancel.setOpaque(true);
        btnCancel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCancelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCancelMouseExited(evt);
            }
        });
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        jPanel2.add(btnCancel);

        btnNew1.setBackground(new java.awt.Color(56, 16, 149));
        btnNew1.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        btnNew1.setForeground(new java.awt.Color(255, 255, 255));
        btnNew1.setText("<html>New <br>Order</html>");
        btnNew1.setToolTipText("");
        btnNew1.setBorderPainted(false);
        btnNew1.setContentAreaFilled(false);
        btnNew1.setOpaque(true);
        btnNew1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnNew1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnNew1MouseExited(evt);
            }
        });
        jPanel2.add(btnNew1);

        btnSave.setBackground(new java.awt.Color(56, 16, 149));
        btnSave.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        btnSave.setForeground(new java.awt.Color(255, 255, 255));
        btnSave.setText("<html>Save<br>Order</html>");
        btnSave.setToolTipText("");
        btnSave.setBorderPainted(false);
        btnSave.setContentAreaFilled(false);
        btnSave.setOpaque(true);
        btnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveMouseExited(evt);
            }
        });
        jPanel2.add(btnSave);

        btnPending.setBackground(new java.awt.Color(56, 16, 149));
        btnPending.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        btnPending.setForeground(new java.awt.Color(255, 255, 255));
        btnPending.setText("Debts");
        btnPending.setToolTipText("");
        btnPending.setBorderPainted(false);
        btnPending.setContentAreaFilled(false);
        btnPending.setFocusable(false);
        btnPending.setOpaque(true);
        btnPending.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnPendingMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnPendingMouseExited(evt);
            }
        });
        jPanel2.add(btnPending);

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Search_20px.png"))); // NOI18N

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(56, 16, 149));
        jLabel5.setText("Order No.");

        lblSaleNo2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblSaleNo2.setText("1000000");

        javax.swing.GroupLayout PaymentsLayout = new javax.swing.GroupLayout(Payments);
        Payments.setLayout(PaymentsLayout);
        PaymentsLayout.setHorizontalGroup(
            PaymentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PaymentsLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(PaymentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PaymentsLayout.createSequentialGroup()
                        .addGroup(PaymentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(PaymentsLayout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(3, 3, 3))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PaymentsLayout.createSequentialGroup()
                                .addComponent(jLabel15)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(PaymentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtSearch)
                            .addComponent(txtSupplier))
                        .addGap(58, 58, 58)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblSaleNo2, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20))
                    .addGroup(PaymentsLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 782, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(PaymentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(PaymentsLayout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(txtTotal2, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(10, 10, 10))))
        );
        PaymentsLayout.setVerticalGroup(
            PaymentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PaymentsLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(PaymentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(PaymentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblSaleNo2)
                        .addComponent(jLabel5)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PaymentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSupplier, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PaymentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PaymentsLayout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtTotal2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35))
                    .addGroup(PaymentsLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 630, Short.MAX_VALUE)
                        .addGap(10, 10, 10))))
        );

        Body.add(Payments, "card2");

        ReportsTab.setBackground(new java.awt.Color(255, 255, 255));
        ReportsTab.setFont(new java.awt.Font("Tahoma", 0, 22)); // NOI18N

        InvoiceReport.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout InvoiceReportLayout = new javax.swing.GroupLayout(InvoiceReport);
        InvoiceReport.setLayout(InvoiceReportLayout);
        InvoiceReportLayout.setHorizontalGroup(
            InvoiceReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1039, Short.MAX_VALUE)
        );
        InvoiceReportLayout.setVerticalGroup(
            InvoiceReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 683, Short.MAX_VALUE)
        );

        ReportsTab.addTab("Invoices", InvoiceReport);

        PaymentReport.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout PaymentReportLayout = new javax.swing.GroupLayout(PaymentReport);
        PaymentReport.setLayout(PaymentReportLayout);
        PaymentReportLayout.setHorizontalGroup(
            PaymentReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1039, Short.MAX_VALUE)
        );
        PaymentReportLayout.setVerticalGroup(
            PaymentReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 683, Short.MAX_VALUE)
        );

        ReportsTab.addTab("Payments", PaymentReport);

        DeliveryReport.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout DeliveryReportLayout = new javax.swing.GroupLayout(DeliveryReport);
        DeliveryReport.setLayout(DeliveryReportLayout);
        DeliveryReportLayout.setHorizontalGroup(
            DeliveryReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1039, Short.MAX_VALUE)
        );
        DeliveryReportLayout.setVerticalGroup(
            DeliveryReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 683, Short.MAX_VALUE)
        );

        ReportsTab.addTab("Delivery", DeliveryReport);

        SalesReport.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout SalesReportLayout = new javax.swing.GroupLayout(SalesReport);
        SalesReport.setLayout(SalesReportLayout);
        SalesReportLayout.setHorizontalGroup(
            SalesReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1039, Short.MAX_VALUE)
        );
        SalesReportLayout.setVerticalGroup(
            SalesReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 683, Short.MAX_VALUE)
        );

        ReportsTab.addTab("Sales", SalesReport);

        javax.swing.GroupLayout ReportsLayout = new javax.swing.GroupLayout(Reports);
        Reports.setLayout(ReportsLayout);
        ReportsLayout.setHorizontalGroup(
            ReportsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ReportsTab, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        ReportsLayout.setVerticalGroup(
            ReportsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ReportsTab, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        Body.add(Reports, "card8");

        Inventory.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 22)); // NOI18N

        StockTab.setBackground(new java.awt.Color(255, 255, 255));

        jScrollPane5.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tblStock.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        tblStock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"1", "Savanna Cement", null, "6", "600"},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "ID", "Item", "Unit", "Qty", "Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblStock.setGridColor(new java.awt.Color(153, 153, 153));
        tblStock.setRowHeight(25);
        tblStock.getTableHeader().setResizingAllowed(false);
        tblStock.getTableHeader().setReorderingAllowed(false);
        tblStock.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblStockMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(tblStock);
        if (tblStock.getColumnModel().getColumnCount() > 0) {
            tblStock.getColumnModel().getColumn(0).setResizable(false);
            tblStock.getColumnModel().getColumn(1).setResizable(false);
            tblStock.getColumnModel().getColumn(1).setPreferredWidth(600);
            tblStock.getColumnModel().getColumn(2).setResizable(false);
            tblStock.getColumnModel().getColumn(2).setPreferredWidth(100);
            tblStock.getColumnModel().getColumn(3).setResizable(false);
            tblStock.getColumnModel().getColumn(3).setPreferredWidth(100);
            tblStock.getColumnModel().getColumn(4).setResizable(false);
            tblStock.getColumnModel().getColumn(4).setPreferredWidth(120);
        }

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Search_20px.png"))); // NOI18N

        txtSearch3.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        txtSearch3.setToolTipText("Search by item name");
        txtSearch3.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txtSearch3CaretUpdate(evt);
            }
        });

        btnStockReport.setBackground(new java.awt.Color(103, 33, 122));
        btnStockReport.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        btnStockReport.setForeground(new java.awt.Color(255, 255, 255));
        btnStockReport.setText("<html>Generate<br>Report</html>");
        btnStockReport.setBorderPainted(false);
        btnStockReport.setContentAreaFilled(false);
        btnStockReport.setOpaque(true);
        btnStockReport.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnStockReportMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnStockReportMouseExited(evt);
            }
        });

        javax.swing.GroupLayout StockTabLayout = new javax.swing.GroupLayout(StockTab);
        StockTab.setLayout(StockTabLayout);
        StockTabLayout.setHorizontalGroup(
            StockTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(StockTabLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(StockTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(StockTabLayout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addGap(10, 10, 10)
                        .addComponent(txtSearch3, javax.swing.GroupLayout.DEFAULT_SIZE, 848, Short.MAX_VALUE))
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 878, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnStockReport, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6))
        );
        StockTabLayout.setVerticalGroup(
            StockTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(StockTabLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(StockTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSearch3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(StockTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 623, Short.MAX_VALUE)
                    .addGroup(StockTabLayout.createSequentialGroup()
                        .addComponent(btnStockReport, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Stock", StockTab);

        LowStock.setBackground(new java.awt.Color(255, 255, 255));

        jScrollPane10.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Low Stock", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N

        tblLow.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        tblLow.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Item", "Unit", "Remaining Qty"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblLow.setMinimumSize(new java.awt.Dimension(30, 64));
        tblLow.setRowHeight(25);
        tblLow.getTableHeader().setReorderingAllowed(false);
        jScrollPane10.setViewportView(tblLow);
        if (tblLow.getColumnModel().getColumnCount() > 0) {
            tblLow.getColumnModel().getColumn(0).setResizable(false);
            tblLow.getColumnModel().getColumn(0).setPreferredWidth(75);
            tblLow.getColumnModel().getColumn(1).setResizable(false);
            tblLow.getColumnModel().getColumn(1).setPreferredWidth(300);
            tblLow.getColumnModel().getColumn(2).setResizable(false);
            tblLow.getColumnModel().getColumn(2).setPreferredWidth(100);
            tblLow.getColumnModel().getColumn(3).setResizable(false);
            tblLow.getColumnModel().getColumn(3).setPreferredWidth(150);
        }

        jScrollPane11.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "New Order", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N

        tblOrder.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        tblOrder.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Item", "Qty", "Note"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblOrder.setMinimumSize(new java.awt.Dimension(30, 64));
        tblOrder.setRowHeight(25);
        tblOrder.getTableHeader().setReorderingAllowed(false);
        tblOrder.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblOrderMouseClicked(evt);
            }
        });
        jScrollPane11.setViewportView(tblOrder);
        if (tblOrder.getColumnModel().getColumnCount() > 0) {
            tblOrder.getColumnModel().getColumn(0).setResizable(false);
            tblOrder.getColumnModel().getColumn(0).setPreferredWidth(50);
            tblOrder.getColumnModel().getColumn(1).setResizable(false);
            tblOrder.getColumnModel().getColumn(1).setPreferredWidth(300);
            tblOrder.getColumnModel().getColumn(2).setResizable(false);
            tblOrder.getColumnModel().getColumn(2).setPreferredWidth(100);
            tblOrder.getColumnModel().getColumn(3).setResizable(false);
            tblOrder.getColumnModel().getColumn(3).setPreferredWidth(300);
        }

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Make order", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.ABOVE_TOP, new java.awt.Font("Tahoma", 0, 12))); // NOI18N

        jLabel27.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel27.setText("Item");

        jLabel30.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel30.setText("Note");

        txtItem3.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        txtDes.setColumns(20);
        txtDes.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        txtDes.setLineWrap(true);
        txtDes.setRows(2);
        txtDes.setTabSize(4);
        txtDes.setWrapStyleWord(true);
        jScrollPane3.setViewportView(txtDes);

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 10, 10));

        btnCancel5.setBackground(new java.awt.Color(56, 16, 149));
        btnCancel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnCancel5.setForeground(new java.awt.Color(255, 255, 255));
        btnCancel5.setText("Clear");
        btnCancel5.setBorderPainted(false);
        btnCancel5.setContentAreaFilled(false);
        btnCancel5.setMinimumSize(new java.awt.Dimension(125, 50));
        btnCancel5.setOpaque(true);
        btnCancel5.setPreferredSize(new java.awt.Dimension(125, 50));
        btnCancel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCancel5MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCancel5MouseExited(evt);
            }
        });
        btnCancel5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancel5ActionPerformed(evt);
            }
        });
        jPanel13.add(btnCancel5);

        btnSave6.setBackground(new java.awt.Color(56, 16, 149));
        btnSave6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSave6.setForeground(new java.awt.Color(255, 255, 255));
        btnSave6.setText("Save");
        btnSave6.setBorderPainted(false);
        btnSave6.setContentAreaFilled(false);
        btnSave6.setMinimumSize(new java.awt.Dimension(125, 50));
        btnSave6.setOpaque(true);
        btnSave6.setPreferredSize(new java.awt.Dimension(125, 50));
        btnSave6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSave6MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSave6MouseExited(evt);
            }
        });
        btnSave6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSave6ActionPerformed(evt);
            }
        });
        jPanel13.add(btnSave6);

        btnDel4.setBackground(new java.awt.Color(220, 0, 0));
        btnDel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnDel4.setForeground(new java.awt.Color(255, 255, 255));
        btnDel4.setText("Delete");
        btnDel4.setBorderPainted(false);
        btnDel4.setContentAreaFilled(false);
        btnDel4.setMinimumSize(new java.awt.Dimension(125, 50));
        btnDel4.setOpaque(true);
        btnDel4.setPreferredSize(new java.awt.Dimension(125, 50));
        btnDel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnDel4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnDel4MouseExited(evt);
            }
        });
        btnDel4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDel4ActionPerformed(evt);
            }
        });
        jPanel13.add(btnDel4);

        txtItem4.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        jLabel28.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel28.setText("Quantity");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel28)
                        .addGap(18, 18, 18)
                        .addComponent(txtItem4))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel27)
                            .addComponent(jLabel30))
                        .addGap(44, 44, 44)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtItem3, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 404, Short.MAX_VALUE))))
                    .addComponent(jPanel13, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtItem3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtItem4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout LowStockLayout = new javax.swing.GroupLayout(LowStock);
        LowStock.setLayout(LowStockLayout);
        LowStockLayout.setHorizontalGroup(
            LowStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LowStockLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 502, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(LowStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        LowStockLayout.setVerticalGroup(
            LowStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, LowStockLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(LowStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 657, Short.MAX_VALUE)
                    .addGroup(LowStockLayout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Orders", LowStock);

        ManageItems.setBackground(new java.awt.Color(255, 255, 255));

        tblManageStock.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        tblManageStock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, "Red Oxide", "kg", "1", null, null},
                {null, "3/4\" Steel Nails", "pcs", "2", null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "ID", "Item", "Unit", "Qty", "Price", "Low Qty"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblManageStock.setMaximumSize(new java.awt.Dimension(2147483647, 2147483647));
        tblManageStock.setMinimumSize(new java.awt.Dimension(30, 64));
        tblManageStock.setRowHeight(25);
        tblManageStock.getTableHeader().setReorderingAllowed(false);
        tblManageStock.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblManageStockMouseClicked(evt);
            }
        });
        jScrollPane9.setViewportView(tblManageStock);
        if (tblManageStock.getColumnModel().getColumnCount() > 0) {
            tblManageStock.getColumnModel().getColumn(0).setResizable(false);
            tblManageStock.getColumnModel().getColumn(0).setPreferredWidth(100);
            tblManageStock.getColumnModel().getColumn(1).setResizable(false);
            tblManageStock.getColumnModel().getColumn(1).setPreferredWidth(400);
            tblManageStock.getColumnModel().getColumn(2).setResizable(false);
            tblManageStock.getColumnModel().getColumn(2).setPreferredWidth(200);
            tblManageStock.getColumnModel().getColumn(3).setResizable(false);
            tblManageStock.getColumnModel().getColumn(3).setPreferredWidth(200);
            tblManageStock.getColumnModel().getColumn(4).setResizable(false);
            tblManageStock.getColumnModel().getColumn(4).setPreferredWidth(200);
            tblManageStock.getColumnModel().getColumn(5).setResizable(false);
            tblManageStock.getColumnModel().getColumn(5).setPreferredWidth(200);
        }

        jTextField16.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jTextField16.setToolTipText("Search by item name");
        jTextField16.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField16CaretUpdate(evt);
            }
        });

        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Search_20px.png"))); // NOI18N
        jLabel25.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 10, 10));

        btnCancel3.setBackground(new java.awt.Color(56, 16, 149));
        btnCancel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnCancel3.setForeground(new java.awt.Color(255, 255, 255));
        btnCancel3.setText("Clear");
        btnCancel3.setBorderPainted(false);
        btnCancel3.setContentAreaFilled(false);
        btnCancel3.setMinimumSize(new java.awt.Dimension(125, 50));
        btnCancel3.setOpaque(true);
        btnCancel3.setPreferredSize(new java.awt.Dimension(125, 50));
        btnCancel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCancel3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCancel3MouseExited(evt);
            }
        });
        btnCancel3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancel3ActionPerformed(evt);
            }
        });
        jPanel12.add(btnCancel3);

        btnUpdate2.setBackground(new java.awt.Color(56, 16, 149));
        btnUpdate2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnUpdate2.setForeground(new java.awt.Color(255, 255, 255));
        btnUpdate2.setText("Update");
        btnUpdate2.setBorderPainted(false);
        btnUpdate2.setContentAreaFilled(false);
        btnUpdate2.setMinimumSize(new java.awt.Dimension(125, 50));
        btnUpdate2.setOpaque(true);
        btnUpdate2.setPreferredSize(new java.awt.Dimension(125, 50));
        btnUpdate2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnUpdate2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnUpdate2MouseExited(evt);
            }
        });
        btnUpdate2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdate2ActionPerformed(evt);
            }
        });
        jPanel12.add(btnUpdate2);

        btnDel3.setBackground(new java.awt.Color(220, 0, 0));
        btnDel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnDel3.setForeground(new java.awt.Color(255, 255, 255));
        btnDel3.setText("Delete");
        btnDel3.setBorderPainted(false);
        btnDel3.setContentAreaFilled(false);
        btnDel3.setMinimumSize(new java.awt.Dimension(125, 50));
        btnDel3.setOpaque(true);
        btnDel3.setPreferredSize(new java.awt.Dimension(125, 50));
        btnDel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnDel3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnDel3MouseExited(evt);
            }
        });
        btnDel3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDel3ActionPerformed(evt);
            }
        });
        jPanel12.add(btnDel3);

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel22.setText("Quantity");

        jLabel32.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel32.setText("Item");

        txtItem.setEditable(false);
        txtItem.setBackground(new java.awt.Color(255, 255, 255));
        txtItem.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        txtItem.setToolTipText("Select item from the table");
        txtItem.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel38.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel38.setText("Price");

        jLabel41.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel41.setText("Low Qty");

        txtQty.setBackground(new java.awt.Color(255, 255, 255));

        txtPrice.setBackground(new java.awt.Color(255, 255, 255));

        txtPrice1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel33.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel33.setText("Unit");

        txtUnit1.setEditable(false);
        txtUnit1.setBackground(new java.awt.Color(255, 255, 255));
        txtUnit1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        txtUnit1.setToolTipText("Select item from the table");
        txtUnit1.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel11Layout.createSequentialGroup()
                                .addComponent(jLabel41)
                                .addGap(13, 13, 13)
                                .addComponent(txtPrice1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel11Layout.createSequentialGroup()
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel22)
                                    .addComponent(jLabel38))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtPrice, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtQty, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel11Layout.createSequentialGroup()
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel33)
                                    .addComponent(jLabel32))
                                .addGap(39, 39, 39)
                                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtItem)
                                    .addComponent(txtUnit1))))
                        .addGap(10, 10, 10))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                        .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtItem, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUnit1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtQty, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPrice1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel41, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout ManageItemsLayout = new javax.swing.GroupLayout(ManageItems);
        ManageItems.setLayout(ManageItemsLayout);
        ManageItemsLayout.setHorizontalGroup(
            ManageItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ManageItemsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ManageItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 524, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ManageItemsLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField16)))
                .addContainerGap())
        );
        ManageItemsLayout.setVerticalGroup(
            ManageItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ManageItemsLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(ManageItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(ManageItemsLayout.createSequentialGroup()
                        .addGroup(ManageItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 613, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Update Stock", ManageItems);

        DeliveryTab.setBackground(new java.awt.Color(255, 255, 255));

        tblDelivery.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        tblDelivery.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Item", "Unit", "Qty", "Low qty", "Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblDelivery.setMaximumSize(new java.awt.Dimension(2147483647, 2147483647));
        tblDelivery.setMinimumSize(new java.awt.Dimension(30, 65));
        tblDelivery.setRowHeight(25);
        tblDelivery.getTableHeader().setReorderingAllowed(false);
        tblDelivery.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblDeliveryMouseClicked(evt);
            }
        });
        jScrollPane7.setViewportView(tblDelivery);
        if (tblDelivery.getColumnModel().getColumnCount() > 0) {
            tblDelivery.getColumnModel().getColumn(0).setResizable(false);
            tblDelivery.getColumnModel().getColumn(0).setPreferredWidth(80);
            tblDelivery.getColumnModel().getColumn(1).setResizable(false);
            tblDelivery.getColumnModel().getColumn(1).setPreferredWidth(300);
            tblDelivery.getColumnModel().getColumn(2).setResizable(false);
            tblDelivery.getColumnModel().getColumn(2).setPreferredWidth(100);
            tblDelivery.getColumnModel().getColumn(3).setResizable(false);
            tblDelivery.getColumnModel().getColumn(3).setPreferredWidth(100);
            tblDelivery.getColumnModel().getColumn(4).setResizable(false);
            tblDelivery.getColumnModel().getColumn(4).setPreferredWidth(100);
            tblDelivery.getColumnModel().getColumn(5).setResizable(false);
            tblDelivery.getColumnModel().getColumn(5).setPreferredWidth(200);
        }

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 10, 10));

        btnClr1.setBackground(new java.awt.Color(56, 16, 149));
        btnClr1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnClr1.setForeground(new java.awt.Color(255, 255, 255));
        btnClr1.setText("Clear");
        btnClr1.setBorderPainted(false);
        btnClr1.setContentAreaFilled(false);
        btnClr1.setMinimumSize(new java.awt.Dimension(125, 50));
        btnClr1.setOpaque(true);
        btnClr1.setPreferredSize(new java.awt.Dimension(125, 50));
        btnClr1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnClr1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnClr1MouseExited(evt);
            }
        });
        btnClr1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClr1ActionPerformed(evt);
            }
        });
        jPanel7.add(btnClr1);

        btnSave5.setBackground(new java.awt.Color(56, 16, 149));
        btnSave5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSave5.setForeground(new java.awt.Color(255, 255, 255));
        btnSave5.setText("Add");
        btnSave5.setBorderPainted(false);
        btnSave5.setContentAreaFilled(false);
        btnSave5.setMinimumSize(new java.awt.Dimension(125, 50));
        btnSave5.setOpaque(true);
        btnSave5.setPreferredSize(new java.awt.Dimension(125, 50));
        btnSave5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSave5MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSave5MouseExited(evt);
            }
        });
        btnSave5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSave5ActionPerformed(evt);
            }
        });
        jPanel7.add(btnSave5);

        btnUpdate.setBackground(new java.awt.Color(56, 16, 149));
        btnUpdate.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnUpdate.setForeground(new java.awt.Color(255, 255, 255));
        btnUpdate.setText("Update");
        btnUpdate.setBorderPainted(false);
        btnUpdate.setContentAreaFilled(false);
        btnUpdate.setMinimumSize(new java.awt.Dimension(125, 50));
        btnUpdate.setOpaque(true);
        btnUpdate.setPreferredSize(new java.awt.Dimension(125, 50));
        btnUpdate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnUpdateMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnUpdateMouseExited(evt);
            }
        });
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        jPanel7.add(btnUpdate);

        btnDel.setBackground(new java.awt.Color(220, 0, 0));
        btnDel.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnDel.setForeground(new java.awt.Color(255, 255, 255));
        btnDel.setText("Remove");
        btnDel.setBorderPainted(false);
        btnDel.setContentAreaFilled(false);
        btnDel.setMinimumSize(new java.awt.Dimension(125, 50));
        btnDel.setOpaque(true);
        btnDel.setPreferredSize(new java.awt.Dimension(125, 50));
        btnDel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnDelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnDelMouseExited(evt);
            }
        });
        btnDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelActionPerformed(evt);
            }
        });
        jPanel7.add(btnDel);

        btnSave2.setBackground(new java.awt.Color(56, 16, 149));
        btnSave2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSave2.setForeground(new java.awt.Color(255, 255, 255));
        btnSave2.setText("Save All");
        btnSave2.setBorderPainted(false);
        btnSave2.setContentAreaFilled(false);
        btnSave2.setMinimumSize(new java.awt.Dimension(125, 50));
        btnSave2.setOpaque(true);
        btnSave2.setPreferredSize(new java.awt.Dimension(125, 50));
        btnSave2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSave2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSave2MouseExited(evt);
            }
        });
        btnSave2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSave2ActionPerformed(evt);
            }
        });
        jPanel7.add(btnSave2);

        jLabel26.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel26.setText("Item");

        txtItem1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        jLabel29.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel29.setText("Unit");

        txtUnt.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        jLabel37.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel37.setText("Quantity");

        jLabel39.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel39.setText("Low qty");

        jLabel45.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel45.setText("Price");

        txtLowCnt.setBackground(new java.awt.Color(255, 255, 255));

        txtQty2.setBackground(new java.awt.Color(255, 255, 255));

        txtPrice2.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel37)
                            .addComponent(jLabel39)
                            .addComponent(jLabel45))
                        .addGap(20, 20, 20)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPrice2, javax.swing.GroupLayout.DEFAULT_SIZE, 370, Short.MAX_VALUE)
                            .addComponent(txtLowCnt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtQty2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtUnt)
                            .addComponent(txtItem1))))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(txtItem1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtUnt, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtQty2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel39, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLowCnt, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel45, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(txtPrice2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout DeliveryTabLayout = new javax.swing.GroupLayout(DeliveryTab);
        DeliveryTab.setLayout(DeliveryTabLayout);
        DeliveryTabLayout.setHorizontalGroup(
            DeliveryTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DeliveryTabLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 529, Short.MAX_VALUE)
                .addContainerGap())
        );
        DeliveryTabLayout.setVerticalGroup(
            DeliveryTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DeliveryTabLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(DeliveryTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 650, Short.MAX_VALUE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("New Item", DeliveryTab);

        CategoryTab.setBackground(new java.awt.Color(255, 255, 255));

        jTable1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"1", "Red Oxide", "1", "kg", null},
                {"2", "3/4\" Steel Nails", "2", "pcs", null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "ID", "Item", "Qty", "Subunit", "Qty"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setGridColor(new java.awt.Color(153, 153, 153));
        jTable1.setMinimumSize(new java.awt.Dimension(30, 64));
        jTable1.setRowHeight(25);
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(50);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(200);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(4).setResizable(false);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(100);
        }

        jTextField5.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jTextField5.setToolTipText("Search by item name");

        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Search_20px.png"))); // NOI18N
        jLabel23.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel18.setText("Sub unit");

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel20.setText("Quantity");

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel19.setText("Item");

        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Search_20px.png"))); // NOI18N
        jLabel17.setToolTipText("Search item");
        jLabel17.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 10, 10));

        btnCancel4.setBackground(new java.awt.Color(56, 16, 149));
        btnCancel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnCancel4.setForeground(new java.awt.Color(255, 255, 255));
        btnCancel4.setText("Clear");
        btnCancel4.setBorderPainted(false);
        btnCancel4.setContentAreaFilled(false);
        btnCancel4.setMinimumSize(new java.awt.Dimension(125, 50));
        btnCancel4.setOpaque(true);
        btnCancel4.setPreferredSize(new java.awt.Dimension(125, 50));
        btnCancel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCancel4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCancel4MouseExited(evt);
            }
        });
        btnCancel4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancel4ActionPerformed(evt);
            }
        });
        jPanel10.add(btnCancel4);

        btnSave3.setBackground(new java.awt.Color(56, 16, 149));
        btnSave3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSave3.setForeground(new java.awt.Color(255, 255, 255));
        btnSave3.setText("Save");
        btnSave3.setBorderPainted(false);
        btnSave3.setContentAreaFilled(false);
        btnSave3.setMinimumSize(new java.awt.Dimension(125, 50));
        btnSave3.setOpaque(true);
        btnSave3.setPreferredSize(new java.awt.Dimension(125, 50));
        btnSave3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSave3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSave3MouseExited(evt);
            }
        });
        jPanel10.add(btnSave3);

        btnDel1.setBackground(new java.awt.Color(220, 0, 0));
        btnDel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnDel1.setForeground(new java.awt.Color(255, 255, 255));
        btnDel1.setText("Delete");
        btnDel1.setBorderPainted(false);
        btnDel1.setContentAreaFilled(false);
        btnDel1.setMinimumSize(new java.awt.Dimension(125, 50));
        btnDel1.setOpaque(true);
        btnDel1.setPreferredSize(new java.awt.Dimension(125, 50));
        btnDel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnDel1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnDel1MouseExited(evt);
            }
        });
        jPanel10.add(btnDel1);

        txtSubUnit.setBackground(new java.awt.Color(255, 255, 255));

        txtQty1.setBackground(new java.awt.Color(255, 255, 255));

        txtItem2.setEditable(false);
        txtItem2.setBackground(new java.awt.Color(255, 255, 255));
        txtItem2.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        txtItem2.setToolTipText("Click the search button");

        jLabel36.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel36.setText("Quantity");

        txtSubUnit1.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addGap(27, 27, 27))
                    .addComponent(jLabel20, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(txtItem2, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(txtQty1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(13, 13, 13))
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel36)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtSubUnit1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(14, 14, 14))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtSubUnit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtItem2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtQty1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSubUnit, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSubUnit1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout CategoryTabLayout = new javax.swing.GroupLayout(CategoryTab);
        CategoryTab.setLayout(CategoryTabLayout);
        CategoryTabLayout.setHorizontalGroup(
            CategoryTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CategoryTabLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(CategoryTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CategoryTabLayout.createSequentialGroup()
                        .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField5))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CategoryTabLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 515, Short.MAX_VALUE)))
                .addContainerGap())
        );
        CategoryTabLayout.setVerticalGroup(
            CategoryTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CategoryTabLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(CategoryTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(CategoryTabLayout.createSequentialGroup()
                        .addGroup(CategoryTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 613, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Sub-divisions", CategoryTab);

        javax.swing.GroupLayout InventoryLayout = new javax.swing.GroupLayout(Inventory);
        Inventory.setLayout(InventoryLayout);
        InventoryLayout.setHorizontalGroup(
            InventoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        InventoryLayout.setVerticalGroup(
            InventoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        Body.add(Inventory, "card2");

        Contacts.setBackground(new java.awt.Color(255, 255, 255));
        Contacts.setPreferredSize(new java.awt.Dimension(868, 520));

        jTextField12.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jTextField12.setToolTipText("Search by customer name");
        jTextField12.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField12CaretUpdate(evt);
            }
        });

        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Search_20px.png"))); // NOI18N
        jLabel31.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        tblCustomer.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        tblCustomer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Customer Name", "Phone number"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblCustomer.setMinimumSize(new java.awt.Dimension(30, 64));
        tblCustomer.setRowHeight(25);
        tblCustomer.getTableHeader().setReorderingAllowed(false);
        tblCustomer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblCustomerMouseClicked(evt);
            }
        });
        jScrollPane8.setViewportView(tblCustomer);
        if (tblCustomer.getColumnModel().getColumnCount() > 0) {
            tblCustomer.getColumnModel().getColumn(0).setResizable(false);
            tblCustomer.getColumnModel().getColumn(0).setPreferredWidth(50);
            tblCustomer.getColumnModel().getColumn(1).setResizable(false);
            tblCustomer.getColumnModel().getColumn(1).setPreferredWidth(200);
            tblCustomer.getColumnModel().getColumn(2).setResizable(false);
            tblCustomer.getColumnModel().getColumn(2).setPreferredWidth(200);
            tblCustomer.getColumnModel().getColumn(2).setHeaderValue("Phone number");
        }

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 10, 10));

        btnSave4.setBackground(new java.awt.Color(56, 16, 149));
        btnSave4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSave4.setForeground(new java.awt.Color(255, 255, 255));
        btnSave4.setText("Save");
        btnSave4.setBorderPainted(false);
        btnSave4.setContentAreaFilled(false);
        btnSave4.setMinimumSize(new java.awt.Dimension(125, 50));
        btnSave4.setOpaque(true);
        btnSave4.setPreferredSize(new java.awt.Dimension(125, 50));
        btnSave4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSave4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSave4MouseExited(evt);
            }
        });
        btnSave4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSave4ActionPerformed(evt);
            }
        });
        jPanel9.add(btnSave4);

        btnUpdate1.setBackground(new java.awt.Color(56, 16, 149));
        btnUpdate1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnUpdate1.setForeground(new java.awt.Color(255, 255, 255));
        btnUpdate1.setText("Update");
        btnUpdate1.setBorderPainted(false);
        btnUpdate1.setContentAreaFilled(false);
        btnUpdate1.setMinimumSize(new java.awt.Dimension(125, 50));
        btnUpdate1.setOpaque(true);
        btnUpdate1.setPreferredSize(new java.awt.Dimension(125, 50));
        btnUpdate1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnUpdate1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnUpdate1MouseExited(evt);
            }
        });
        btnUpdate1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdate1ActionPerformed(evt);
            }
        });
        jPanel9.add(btnUpdate1);

        btnDel2.setBackground(new java.awt.Color(220, 0, 0));
        btnDel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnDel2.setForeground(new java.awt.Color(255, 255, 255));
        btnDel2.setText("Delete");
        btnDel2.setBorderPainted(false);
        btnDel2.setContentAreaFilled(false);
        btnDel2.setMinimumSize(new java.awt.Dimension(125, 50));
        btnDel2.setOpaque(true);
        btnDel2.setPreferredSize(new java.awt.Dimension(125, 50));
        btnDel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnDel2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnDel2MouseExited(evt);
            }
        });
        btnDel2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDel2ActionPerformed(evt);
            }
        });
        jPanel9.add(btnDel2);

        txtPhone.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        txtPhone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPhoneActionPerformed(evt);
            }
        });

        jLabel34.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel34.setText("Phone number");

        jLabel35.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel35.setText("Customer name");

        txtName.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel34)
                        .addGap(14, 14, 14)
                        .addComponent(txtPhone))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel35)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtName)))
                .addGap(10, 10, 10))
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, 483, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPhone, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout ContactsLayout = new javax.swing.GroupLayout(Contacts);
        Contacts.setLayout(ContactsLayout);
        ContactsLayout.setHorizontalGroup(
            ContactsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ContactsLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(ContactsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ContactsLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(jTextField12, javax.swing.GroupLayout.DEFAULT_SIZE, 475, Short.MAX_VALUE))
                    .addGroup(ContactsLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        ContactsLayout.setVerticalGroup(
            ContactsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ContactsLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(ContactsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(ContactsLayout.createSequentialGroup()
                        .addGroup(ContactsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 654, Short.MAX_VALUE)))
                .addContainerGap())
        );

        Body.add(Contacts, "card2");

        Setting.setBackground(new java.awt.Color(255, 255, 255));

        jPanel19.setBackground(new java.awt.Color(255, 255, 255));
        jPanel19.setMinimumSize(new java.awt.Dimension(795, 25));
        jPanel19.setLayout(new java.awt.GridLayout(1, 0, 30, 30));

        btnChngPass.setBackground(new java.awt.Color(18, 19, 34));
        btnChngPass.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        btnChngPass.setForeground(new java.awt.Color(255, 255, 255));
        btnChngPass.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/icons8-password-reset-30.png"))); // NOI18N
        btnChngPass.setText("Change Password");
        btnChngPass.setBorderPainted(false);
        btnChngPass.setContentAreaFilled(false);
        btnChngPass.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnChngPass.setOpaque(true);
        btnChngPass.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnChngPass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChngPassActionPerformed(evt);
            }
        });
        jPanel19.add(btnChngPass);

        btnMngUser.setBackground(new java.awt.Color(18, 19, 34));
        btnMngUser.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        btnMngUser.setForeground(new java.awt.Color(255, 255, 255));
        btnMngUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/icons8-admin-settings-male-30.png"))); // NOI18N
        btnMngUser.setText("Manage Users");
        btnMngUser.setBorderPainted(false);
        btnMngUser.setContentAreaFilled(false);
        btnMngUser.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnMngUser.setOpaque(true);
        btnMngUser.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnMngUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMngUserActionPerformed(evt);
            }
        });
        jPanel19.add(btnMngUser);

        btnBackup.setBackground(new java.awt.Color(18, 19, 34));
        btnBackup.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        btnBackup.setForeground(new java.awt.Color(255, 255, 255));
        btnBackup.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/icons8-data-backup-30.png"))); // NOI18N
        btnBackup.setText("Backup");
        btnBackup.setBorderPainted(false);
        btnBackup.setContentAreaFilled(false);
        btnBackup.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnBackup.setOpaque(true);
        btnBackup.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnBackup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackupActionPerformed(evt);
            }
        });
        jPanel19.add(btnBackup);

        btnRestore.setBackground(new java.awt.Color(18, 19, 34));
        btnRestore.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        btnRestore.setForeground(new java.awt.Color(255, 255, 255));
        btnRestore.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/icons8-database-restore-30.png"))); // NOI18N
        btnRestore.setText("Restore");
        btnRestore.setBorderPainted(false);
        btnRestore.setContentAreaFilled(false);
        btnRestore.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRestore.setOpaque(true);
        btnRestore.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnRestore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRestoreActionPerformed(evt);
            }
        });
        jPanel19.add(btnRestore);

        btnLogout.setBackground(new java.awt.Color(18, 19, 34));
        btnLogout.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        btnLogout.setForeground(new java.awt.Color(255, 255, 255));
        btnLogout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/icons8-exit-30.png"))); // NOI18N
        btnLogout.setText("Logout");
        btnLogout.setBorderPainted(false);
        btnLogout.setContentAreaFilled(false);
        btnLogout.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnLogout.setOpaque(true);
        btnLogout.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });
        jPanel19.add(btnLogout);

        jPanel20.setBackground(new java.awt.Color(255, 255, 255));
        jPanel20.setLayout(new java.awt.CardLayout());

        ChangePassword.setBackground(new java.awt.Color(255, 255, 255));
        ChangePassword.setLayout(new javax.swing.BoxLayout(ChangePassword, javax.swing.BoxLayout.X_AXIS));

        jPanel21.setBackground(new java.awt.Color(255, 255, 255));

        txtcurr.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel21.setText("Current Password");

        jLabel24.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel24.setText("New Password");

        txtnew.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        txtnew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnewActionPerformed(evt);
            }
        });

        txtconfirm.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        jLabel40.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel40.setText("Confirm Password");

        btnUpdatePass1.setBackground(new java.awt.Color(56, 16, 149));
        btnUpdatePass1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnUpdatePass1.setForeground(new java.awt.Color(255, 255, 255));
        btnUpdatePass1.setText("Change");
        btnUpdatePass1.setBorderPainted(false);
        btnUpdatePass1.setContentAreaFilled(false);
        btnUpdatePass1.setOpaque(true);
        btnUpdatePass1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnUpdatePass1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnUpdatePass1MouseExited(evt);
            }
        });
        btnUpdatePass1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdatePass1ActionPerformed(evt);
            }
        });

        jCheckBox5.setBackground(new java.awt.Color(255, 255, 255));
        jCheckBox5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jCheckBox5.setHideActionText(true);
        jCheckBox5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jCheckBox5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Eye_20px.png"))); // NOI18N
        jCheckBox5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox5ActionPerformed(evt);
            }
        });

        jPanel16.setBackground(new java.awt.Color(255, 255, 255));
        jPanel16.setEnabled(false);
        jPanel16.setOpaque(false);

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 150, Short.MAX_VALUE)
        );

        jPanel17.setBackground(new java.awt.Color(255, 255, 255));
        jPanel17.setEnabled(false);
        jPanel17.setOpaque(false);

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 235, Short.MAX_VALUE)
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 150, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel24)
                    .addComponent(jLabel21)
                    .addComponent(jLabel40))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel21Layout.createSequentialGroup()
                        .addComponent(btnUpdatePass1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 477, Short.MAX_VALUE))
                    .addGroup(jPanel21Layout.createSequentialGroup()
                        .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtcurr, javax.swing.GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE)
                            .addComponent(txtnew)
                            .addComponent(txtconfirm))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jCheckBox5)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel21Layout.createSequentialGroup()
                            .addGap(26, 26, 26)
                            .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtcurr, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(20, 20, 20)
                            .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtnew, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(27, 27, 27)
                            .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtconfirm, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jCheckBox5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel21Layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel21Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(btnUpdatePass1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        ChangePassword.add(jPanel21);

        jPanel20.add(ChangePassword, "card2");

        MngUsers.setBackground(new java.awt.Color(255, 255, 255));

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));

        btnSave7.setBackground(new java.awt.Color(56, 16, 149));
        btnSave7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSave7.setForeground(new java.awt.Color(255, 255, 255));
        btnSave7.setText("Save");
        btnSave7.setBorderPainted(false);
        btnSave7.setContentAreaFilled(false);
        btnSave7.setMinimumSize(new java.awt.Dimension(125, 50));
        btnSave7.setOpaque(true);
        btnSave7.setPreferredSize(new java.awt.Dimension(125, 50));
        btnSave7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSave7MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSave7MouseExited(evt);
            }
        });
        btnSave7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSave7ActionPerformed(evt);
            }
        });
        jPanel15.add(btnSave7);

        btnDel5.setBackground(new java.awt.Color(220, 0, 0));
        btnDel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnDel5.setForeground(new java.awt.Color(255, 255, 255));
        btnDel5.setText("Delete");
        btnDel5.setBorderPainted(false);
        btnDel5.setContentAreaFilled(false);
        btnDel5.setMinimumSize(new java.awt.Dimension(125, 50));
        btnDel5.setOpaque(true);
        btnDel5.setPreferredSize(new java.awt.Dimension(125, 50));
        btnDel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnDel5MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnDel5MouseExited(evt);
            }
        });
        btnDel5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDel5ActionPerformed(evt);
            }
        });
        jPanel15.add(btnDel5);

        jLabel42.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel42.setText("Employee name");

        txtName1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        jLabel44.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel44.setText("User Type");
        jLabel44.setToolTipText("");

        jComboBox1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Employee", "Administrator" }));

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel42)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtName1)
                .addGap(10, 10, 10))
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel44)
                .addGap(47, 47, 47)
                .addComponent(jComboBox1, 0, 336, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(21, 21, 21))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel42, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtName1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(378, Short.MAX_VALUE))
        );

        tblCustomer1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        tblCustomer1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Employee Name", "User Type"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblCustomer1.setDragEnabled(true);
        tblCustomer1.setMinimumSize(new java.awt.Dimension(30, 64));
        tblCustomer1.setRowHeight(25);
        tblCustomer1.getTableHeader().setReorderingAllowed(false);
        tblCustomer1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblCustomer1MouseClicked(evt);
            }
        });
        jScrollPane12.setViewportView(tblCustomer1);
        if (tblCustomer1.getColumnModel().getColumnCount() > 0) {
            tblCustomer1.getColumnModel().getColumn(0).setResizable(false);
            tblCustomer1.getColumnModel().getColumn(0).setPreferredWidth(50);
            tblCustomer1.getColumnModel().getColumn(1).setResizable(false);
            tblCustomer1.getColumnModel().getColumn(1).setPreferredWidth(200);
            tblCustomer1.getColumnModel().getColumn(2).setResizable(false);
            tblCustomer1.getColumnModel().getColumn(2).setPreferredWidth(200);
        }

        jTextField13.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jTextField13.setToolTipText("Search by customer name");
        jTextField13.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTextField13CaretUpdate(evt);
            }
        });

        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel43.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Search_20px.png"))); // NOI18N
        jLabel43.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout MngUsersLayout = new javax.swing.GroupLayout(MngUsers);
        MngUsers.setLayout(MngUsersLayout);
        MngUsersLayout.setHorizontalGroup(
            MngUsersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MngUsersLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(MngUsersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(MngUsersLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(jTextField13, javax.swing.GroupLayout.DEFAULT_SIZE, 479, Short.MAX_VALUE))
                    .addGroup(MngUsersLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane12, javax.swing.GroupLayout.DEFAULT_SIZE, 512, Short.MAX_VALUE)))
                .addContainerGap())
        );
        MngUsersLayout.setVerticalGroup(
            MngUsersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MngUsersLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(MngUsersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(MngUsersLayout.createSequentialGroup()
                        .addGroup(MngUsersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane12, javax.swing.GroupLayout.DEFAULT_SIZE, 534, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel20.add(MngUsers, "card3");

        javax.swing.GroupLayout SettingLayout = new javax.swing.GroupLayout(Setting);
        Setting.setLayout(SettingLayout);
        SettingLayout.setHorizontalGroup(
            SettingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SettingLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(SettingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        SettingLayout.setVerticalGroup(
            SettingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SettingLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        Body.add(Setting, "card8");

        getContentPane().add(Body, java.awt.BorderLayout.CENTER);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void txtSupplier1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSupplier1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSupplier1ActionPerformed

    private void txtPhoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPhoneActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPhoneActionPerformed

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnNewActionPerformed

    private void btnSave1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSave1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSave1ActionPerformed

    private void btnPayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPayActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPayActionPerformed

    private void btnVoid2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVoid2MouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnVoid2);
    }//GEN-LAST:event_btnVoid2MouseEntered

    private void btnVoid2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVoid2MouseExited
        // TODO add your handling code here:
        btnColor(btnVoid2);
    }//GEN-LAST:event_btnVoid2MouseExited

    private void btnNew2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNew2MouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnNew2);
    }//GEN-LAST:event_btnNew2MouseEntered

    private void btnNew2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNew2MouseExited
        // TODO add your handling code here:
        btnColor(btnNew2);
    }//GEN-LAST:event_btnNew2MouseExited

    private void btnPending1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPending1MouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnPending1);
    }//GEN-LAST:event_btnPending1MouseEntered

    private void btnPending1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPending1MouseExited
        // TODO add your handling code here:
        btnColor(btnPending1);
    }//GEN-LAST:event_btnPending1MouseExited

    private void btnProcessMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnProcessMouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnProcess);
    }//GEN-LAST:event_btnProcessMouseEntered

    private void btnProcessMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnProcessMouseExited
        // TODO add your handling code here:
        btnColor(btnProcess);
    }//GEN-LAST:event_btnProcessMouseExited

    private void btnCancel2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancel2MouseEntered
        // TODO add your handling code here:
        btnCancel2.setBackground(new Color(241, 94, 94));
    }//GEN-LAST:event_btnCancel2MouseEntered

    private void btnCancel2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancel2MouseExited
        // TODO add your handling code here:
        btnCancel2.setBackground(new Color(200, 0, 0));
    }//GEN-LAST:event_btnCancel2MouseExited

    private void btnVoid1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVoid1MouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnVoid1);
    }//GEN-LAST:event_btnVoid1MouseEntered

    private void btnVoid1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVoid1MouseExited
        // TODO add your handling code here:
        btnColor(btnVoid1);
    }//GEN-LAST:event_btnVoid1MouseExited

    private void btnCancel1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancel1MouseEntered
        // TODO add your handling code here:
        btnCancel1.setBackground(new Color(241, 94, 94));
    }//GEN-LAST:event_btnCancel1MouseEntered

    private void btnCancel1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancel1MouseExited
        // TODO add your handling code here:
        btnCancel1.setBackground(new Color(200, 0, 0));
    }//GEN-LAST:event_btnCancel1MouseExited

    private void btnSave1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSave1MouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnSave1);
    }//GEN-LAST:event_btnSave1MouseEntered

    private void btnSave1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSave1MouseExited
        // TODO add your handling code here:
        btnColor(btnSave1);
    }//GEN-LAST:event_btnSave1MouseExited

    private void btnNewMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNewMouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnNew);
    }//GEN-LAST:event_btnNewMouseEntered

    private void btnNewMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNewMouseExited
        // TODO add your handling code here:
        btnColor(btnNew);
    }//GEN-LAST:event_btnNewMouseExited

    private void btnPayMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPayMouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnPay);
    }//GEN-LAST:event_btnPayMouseEntered

    private void btnPayMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPayMouseExited
        // TODO add your handling code here:
        btnColor(btnPay);
    }//GEN-LAST:event_btnPayMouseExited

    private void btnSaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnSave);
    }//GEN-LAST:event_btnSaveMouseEntered

    private void btnSaveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseExited
        // TODO add your handling code here:
        btnColor(btnSave);
    }//GEN-LAST:event_btnSaveMouseExited

    private void btnNew1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNew1MouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnNew1);
    }//GEN-LAST:event_btnNew1MouseEntered

    private void btnNew1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNew1MouseExited
        // TODO add your handling code here:
        btnColor(btnNew1);
    }//GEN-LAST:event_btnNew1MouseExited

    private void btnCancelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancelMouseEntered
        // TODO add your handling code here:
        btnCancel.setBackground(new Color(241, 94, 94));
    }//GEN-LAST:event_btnCancelMouseEntered

    private void btnCancelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancelMouseExited
        // TODO add your handling code here:
        btnCancel.setBackground(new Color(200, 0, 0));
    }//GEN-LAST:event_btnCancelMouseExited

    private void btnVoidMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVoidMouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnVoid);
    }//GEN-LAST:event_btnVoidMouseEntered

    private void btnVoidMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnVoidMouseExited
        // TODO add your handling code here:
        btnColor(btnVoid);
    }//GEN-LAST:event_btnVoidMouseExited

    private void btnPendingMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPendingMouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnPending);
    }//GEN-LAST:event_btnPendingMouseEntered

    private void btnPendingMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPendingMouseExited
        // TODO add your handling code here:
        btnColor(btnPending);
    }//GEN-LAST:event_btnPendingMouseExited

    private void btnDelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDelMouseEntered
        // TODO add your handling code here:
        btnDel.setBackground(new Color(241, 94, 94));
    }//GEN-LAST:event_btnDelMouseEntered

    private void btnDelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDelMouseExited
        // TODO add your handling code here:
        btnDel.setBackground(new Color(200, 0, 0));
    }//GEN-LAST:event_btnDelMouseExited

    private void btnUpdateMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUpdateMouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnUpdate);
    }//GEN-LAST:event_btnUpdateMouseEntered

    private void btnUpdateMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUpdateMouseExited
        // TODO add your handling code here:
        btnColor(btnUpdate);
    }//GEN-LAST:event_btnUpdateMouseExited

    private void btnSave2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSave2MouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnSave2);
    }//GEN-LAST:event_btnSave2MouseEntered

    private void btnSave2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSave2MouseExited
        // TODO add your handling code here:
        btnColor(btnSave2);
    }//GEN-LAST:event_btnSave2MouseExited

    private void btnSave4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSave4MouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnSave4);
    }//GEN-LAST:event_btnSave4MouseEntered

    private void btnSave4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSave4MouseExited
        // TODO add your handling code here:
        btnColor(btnSave4);
    }//GEN-LAST:event_btnSave4MouseExited

    private void btnUpdate1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUpdate1MouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnUpdate1);
    }//GEN-LAST:event_btnUpdate1MouseEntered

    private void btnUpdate1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUpdate1MouseExited
        // TODO add your handling code here:
        btnColor(btnUpdate1);
    }//GEN-LAST:event_btnUpdate1MouseExited

    private void btnDel2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDel2MouseEntered
        // TODO add your handling code here:
        btnDel2.setBackground(new Color(241, 94, 94));
    }//GEN-LAST:event_btnDel2MouseEntered

    private void btnDel2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDel2MouseExited
        // TODO add your handling code here:
        btnDel2.setBackground(new Color(200, 0, 0));
    }//GEN-LAST:event_btnDel2MouseExited

    private void btnDel3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDel3MouseEntered
        // TODO add your handling code here:
        btnDel3.setBackground(new Color(241, 94, 94));
    }//GEN-LAST:event_btnDel3MouseEntered

    private void btnDel3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDel3MouseExited
        // TODO add your handling code here:
        btnDel3.setBackground(new Color(200, 0, 0));
    }//GEN-LAST:event_btnDel3MouseExited

    private void btnUpdate2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUpdate2MouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnUpdate2);
    }//GEN-LAST:event_btnUpdate2MouseEntered

    private void btnUpdate2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUpdate2MouseExited
        // TODO add your handling code here:
        btnColor(btnUpdate2);
    }//GEN-LAST:event_btnUpdate2MouseExited

    private void btnSave4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSave4ActionPerformed
        // TODO add your handling code here:
        if (txtName.getText() == null || txtPhone.getText() == null) {
            JOptionPane.showMessageDialog(null, "Some fields are empty", null, ERROR_MESSAGE);
        } else {
        Connection con = DBConnection.DBConnect();
        try {
            DBConnection.ps = con.prepareStatement("INSERT INTO contacts (name, phone) VALUES (?, ?)");
            DBConnection.ps.setString(1, txtName.getText());
            DBConnection.ps.setString(2, txtPhone.getText());
            DBConnection.ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Record saved successfully", null, INFORMATION_MESSAGE);
             table.FilterTable(tblCustomer, contacts, jTextField12);
             clrFields(jPanel8);
        } catch (SQLException ex) {
            System.out.println(ex);
            JOptionPane.showMessageDialog(null, "Record could not be saved", null, ERROR_MESSAGE);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println(e);
            }
        }
        }
    }//GEN-LAST:event_btnSave4ActionPerformed

    private void tblCustomerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblCustomerMouseClicked
        // TODO add your handling code here:
        model = (DefaultTableModel) tblCustomer.getModel();
            int selectedRow = tblCustomer.getSelectedRow();
            
            txtName.setText((String) model.getValueAt(selectedRow, 1));
            txtPhone.setText((String) model.getValueAt(selectedRow, 2));
            btnUpdate1.setEnabled(true);
            btnDel2.setEnabled(true);
            btnSave4.setEnabled(false);
    }//GEN-LAST:event_tblCustomerMouseClicked

    private void btnUpdate1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdate1ActionPerformed
        // TODO add your handling code here:
        if (txtName.getText() == null || txtPhone.getText() == null) {
            JOptionPane.showMessageDialog(null, "Some fields are empty", null, ERROR_MESSAGE);
        } else {
            Connection con = DBConnection.DBConnect();
        try {
            model = (DefaultTableModel) tblCustomer.getModel();
            int selectedRow = tblCustomer.getSelectedRow();
            
            DBConnection.ps = con.prepareStatement("UPDATE contacts SET name = ?, phone = ? WHERE customerid = ?");
            DBConnection.ps.setString(1, txtName.getText());
            DBConnection.ps.setString(2, txtPhone.getText());
            DBConnection.ps.setLong(3, (long) model.getValueAt(selectedRow, 0));
            DBConnection.ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Record was successfully updated", null, INFORMATION_MESSAGE);
            table.FilterTable(tblCustomer, contacts, jTextField12);
            clrFields(jPanel8);
            tblCustomer.clearSelection();
            disableOnStart();
            reEnable();
        } catch (SQLException e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, "Record could not be updated. Make sure you have selected a record from the table", null, ERROR_MESSAGE);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
      }
    }//GEN-LAST:event_btnUpdate1ActionPerformed

    private void jTextField12CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField12CaretUpdate
        // TODO add your handling code here:
        table.FilterTable(tblCustomer, contacts, jTextField12);
    }//GEN-LAST:event_jTextField12CaretUpdate

    private void btnDel2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDel2ActionPerformed
        // TODO add your handling code here:
        Connection con = DBConnection.DBConnect();
        model = (DefaultTableModel) tblCustomer.getModel();
        int selectedRow = tblCustomer.getSelectedRow();
        op.delete("DELETE FROM contacts WHERE customerid = ?", (long) model.getValueAt(selectedRow, 0));
        JOptionPane.showMessageDialog(null, "Record deleted successfully", null, INFORMATION_MESSAGE);
        table.FilterTable(tblCustomer, contacts, jTextField12);
        clrFields(jPanel8);
        disableOnStart();
        reEnable(); 
    }//GEN-LAST:event_btnDel2ActionPerformed

    private void txtSearch1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearch1KeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            JOptionPane.showMessageDialog(null, "The typed text is: " + txtSearch1.getText());
        }
    }//GEN-LAST:event_txtSearch1KeyPressed

    private void btnSave6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSave6MouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnSave6);
    }//GEN-LAST:event_btnSave6MouseEntered

    private void btnSave6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSave6MouseExited
        // TODO add your handling code here:
        btnColor(btnSave6);
    }//GEN-LAST:event_btnSave6MouseExited

    private void btnDel4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDel4MouseEntered
        // TODO add your handling code here:
        btnDel4.setBackground(new Color(241, 94, 94));
    }//GEN-LAST:event_btnDel4MouseEntered

    private void btnDel4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDel4MouseExited
        // TODO add your handling code here:
        btnDel4.setBackground(new Color(200, 0, 0));
    }//GEN-LAST:event_btnDel4MouseExited

    private void btnSave6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSave6ActionPerformed
        // TODO add your handling code here:
        if (txtItem3.getText() == null) {
            JOptionPane.showMessageDialog(null, "Ensure that an item has been filled in", null, ERROR_MESSAGE);
        } else {
            Connection con = DBConnection.DBConnect();
        try {
            DBConnection.ps = con.prepareStatement("INSERT INTO orders (item, quantity, description) VALUES (?, ?, ?)");
            DBConnection.ps.setString(1, txtItem3.getText());
            DBConnection.ps.setString(2, txtItem4.getText());
            DBConnection.ps.setString(3, txtDes.getText());
            DBConnection.ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Record saved successfully", null, INFORMATION_MESSAGE);
             table1.FilterTable(tblOrder, orders);
             clrFields(jPanel4);
             reEnable();
        } catch (SQLException ex) {
            System.out.println(ex);
            JOptionPane.showMessageDialog(null, "Record could not be saved", null, ERROR_MESSAGE);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        }
    }//GEN-LAST:event_btnSave6ActionPerformed

    private void btnDel4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDel4ActionPerformed
        // TODO add your handling code here:
        model = (DefaultTableModel) tblOrder.getModel();
        int selectedRow = tblOrder.getSelectedRow();
        op.delete("DELETE FROM orders WHERE orderid = ?", (long) model.getValueAt(selectedRow, 0));
        JOptionPane.showMessageDialog(null, "Order deleted successfully", null, INFORMATION_MESSAGE);
        table1.FilterTable(tblOrder, orders);
        clrFields(jPanel4);
        reEnable();
    }//GEN-LAST:event_btnDel4ActionPerformed

    private void tblOrderMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblOrderMouseClicked
        // TODO add your handling code here:
            model = (DefaultTableModel) tblOrder.getModel();
            int selectedRow = tblOrder.getSelectedRow();
            
            txtItem3.setText((String) model.getValueAt(selectedRow, 1));
            txtItem4.setText(String.valueOf((int) model.getValueAt(selectedRow, 2)));
            txtDes.setText((String) model.getValueAt(selectedRow, 3));
            btnDel4.setEnabled(true);
            btnSave6.setEnabled(false);
    }//GEN-LAST:event_tblOrderMouseClicked

    private void btnContactsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnContactsActionPerformed
        // TODO add your handling code here:
        ChangeColor(Menu, btnContacts);
        TPanel(Body, Contacts);
         table.FilterTable(tblCustomer, contacts, jTextField12);
    }//GEN-LAST:event_btnContactsActionPerformed

    private void btnStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStockActionPerformed
        // TODO add your handling code here:
        ChangeColor(Menu, btnStock);
        TPanel(Body, Inventory);
        table1.FilterTable(tblOrder, orders);
        table1.FilterTable(tblLow, lowQty);
        table.FilterTable(tblStock, stock, txtSearch3);
        table.FilterTable(tblManageStock, stock, jTextField16);
        table1.FilterTable(tblDelivery, delivery);
    }//GEN-LAST:event_btnStockActionPerformed

    private void btnReportsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReportsActionPerformed
        // TODO add your handling code here:
        ChangeColor(Menu, btnReports);
        TPanel(Body, Reports);
    }//GEN-LAST:event_btnReportsActionPerformed

    private void btnPaymentsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPaymentsActionPerformed
        // TODO add your handling code here:
        ChangeColor(Menu, btnPayments);
        TPanel(Body, Payments);
    }//GEN-LAST:event_btnPaymentsActionPerformed

    private void btnInvoiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInvoiceActionPerformed
        // TODO add your handling code here:
        ChangeColor(Menu, btnInvoice);
        TPanel(Body, Invoice);
    }//GEN-LAST:event_btnInvoiceActionPerformed

    private void btnSaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaleActionPerformed
        // TODO add your handling code here:
        ChangeColor(Menu, btnSale);
        TPanel(Body, NewSale);
        
        table.FilterTable(tblSale1, itemSearch, txtSearch2);
    }//GEN-LAST:event_btnSaleActionPerformed

    private void btnStockReportMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnStockReportMouseEntered
        // TODO add your handling code here:
        btnStockReport.setBackground(new Color(153, 107, 166));
    }//GEN-LAST:event_btnStockReportMouseEntered

    private void btnStockReportMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnStockReportMouseExited
        // TODO add your handling code here:
        btnStockReport.setBackground(new Color(103, 33, 122));
    }//GEN-LAST:event_btnStockReportMouseExited

    private void btnSettingsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSettingsActionPerformed
        // TODO add your handling code here:
        ChangeColor(Menu, btnSettings);
        TPanel(Body, Setting);
        table.FilterTable(tblCustomer1, user, jTextField13);
    }//GEN-LAST:event_btnSettingsActionPerformed

    private void btnNew2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNew2ActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_btnNew2ActionPerformed

    private void btnCancel2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancel2ActionPerformed
        // TODO add your handling code here:
        clrTable(tblSale);
        disableOnStart();
    }//GEN-LAST:event_btnCancel2ActionPerformed

    private void tblSaleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblSaleMouseClicked
        // TODO add your handling code here:
        btnVoid2.setEnabled(true);
        tblSale1.clearSelection();
    }//GEN-LAST:event_tblSaleMouseClicked

    private void btnCancel1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancel1ActionPerformed
        // TODO add your handling code here:
        clrTable(tblInvoice);
        disableOnStart();
    }//GEN-LAST:event_btnCancel1ActionPerformed

    private void tblInvoiceMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblInvoiceMouseClicked
        // TODO add your handling code here:
        btnVoid1.setEnabled(true);
    }//GEN-LAST:event_tblInvoiceMouseClicked

    private void tblPaymentMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblPaymentMouseClicked
        // TODO add your handling code here:
        btnVoid.setEnabled(true);
    }//GEN-LAST:event_tblPaymentMouseClicked

    private void btnVoidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoidActionPerformed
        // TODO add your handling code here:
        disableOnStart();
    }//GEN-LAST:event_btnVoidActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        // TODO add your handling code here:
        clrTable(tblPayment);
        disableOnStart();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnVoid1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoid1ActionPerformed
        // TODO add your handling code here:
        disableOnStart();
    }//GEN-LAST:event_btnVoid1ActionPerformed

    private void btnVoid2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoid2ActionPerformed
        // TODO add your handling code here:
        model = (DefaultTableModel) tblSale.getModel();
        int selectedRow = tblSale.getSelectedRow();
        model.removeRow(selectedRow);
        disableOnStart();
        reEnable();
    }//GEN-LAST:event_btnVoid2ActionPerformed

    private void tblStockMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblStockMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tblStockMouseClicked

    private void tblManageStockMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblManageStockMouseClicked
        // TODO add your handling code here:
        try {
            model = (DefaultTableModel) tblManageStock.getModel();
            int selectedRow = tblManageStock.getSelectedRow();
            
            txtItem.setText((String) model.getValueAt(selectedRow, 1));
            txtUnit1.setText((String) model.getValueAt(selectedRow, 2));
            txtPrice.setValue(Integer.parseInt(model.getValueAt(selectedRow, 4).toString()));
            txtPrice1.setValue(Integer.parseInt(model.getValueAt(selectedRow, 5).toString()));
        btnDel3.setEnabled(true);
        btnUpdate2.setEnabled(true);
        } catch (NumberFormatException e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_tblManageStockMouseClicked

    private void tblDeliveryMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblDeliveryMouseClicked
        // TODO add your handling code here:
        try {
            model = (DefaultTableModel) tblDelivery.getModel();
            int selectedRow = tblDelivery.getSelectedRow();
            
        txtItem1.setText((String) model.getValueAt(selectedRow, 1));
        txtUnt.setText((String) model.getValueAt(selectedRow, 2));
        txtQty2.setValue(Integer.parseInt(model.getValueAt(selectedRow, 3).toString()));
        txtLowCnt.setValue(Integer.parseInt(model.getValueAt(selectedRow, 4).toString()));
        txtPrice2.setValue(Integer.parseInt(model.getValueAt(selectedRow, 5).toString()));
        btnDel.setEnabled(true);
        btnUpdate.setEnabled(true);
        btnSave5.setEnabled(false);
        } catch (NumberFormatException e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_tblDeliveryMouseClicked

    private void btnDel3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDel3ActionPerformed
        // TODO add your handling code here:
        if ( JOptionPane.showConfirmDialog(null, "Do you wish to delete the item? The process cannot be reversed.", null, YES_NO_OPTION, WARNING_MESSAGE) == JOptionPane.YES_OPTION) {
            // Check if the stock level is 0
            model = (DefaultTableModel) tblManageStock.getModel();
            int selectedRow = tblManageStock.getSelectedRow();
            
            if (((int) model.getValueAt(selectedRow, 3)) > 0) {
                JOptionPane.showMessageDialog(null, "The item cannot be deleted. Ensure the stock level is depleted first.", null, INFORMATION_MESSAGE);
            } else{
                op.delete("DELETE FROM stock WHERE stockid = ?", (long) model.getValueAt(selectedRow, 0));
                JOptionPane.showMessageDialog(null, "Item deleted successfully", null, INFORMATION_MESSAGE);
                table.FilterTable(tblManageStock, stock, jTextField16);
                clrFields(jPanel11);
                reEnable();
            }
        }
    }//GEN-LAST:event_btnDel3ActionPerformed

    private void txtSearch3CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtSearch3CaretUpdate
        // TODO add your handling code here:
        table.FilterTable(tblStock, stock, txtSearch3);
    }//GEN-LAST:event_txtSearch3CaretUpdate

    private void txtnewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnewActionPerformed

    private void btnUpdatePass1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUpdatePass1MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnUpdatePass1MouseEntered

    private void btnUpdatePass1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnUpdatePass1MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnUpdatePass1MouseExited

    private void btnUpdatePass1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdatePass1ActionPerformed
        // TODO add your handling code here:
        // Check if there are any empty fields
        if (txtcurr.getPassword() == null || txtnew.getPassword() == null || txtconfirm.getPassword() == null) {
            JOptionPane.showMessageDialog(null, "Some fields are empty", null, ERROR_MESSAGE);
        } else {
            // Confirming the existing password
            Connection con = DBConnection.DBConnect();
            try {
                con.setAutoCommit(false);
                DBConnection.ps = con.prepareStatement("SELECT * FROM user WHERE userName = ? AND password = ?");
                DBConnection.ps.setString(1, jLabel7.getText());
                DBConnection.ps.setString(2, String.valueOf(txtcurr.getPassword()));
                DBConnection.rs = DBConnection.ps.executeQuery();
                if (DBConnection.rs.next()) {
                        // Ensure the password is long enough
                        if (txtnew.getPassword().length >= 4) {
                            // Confirm if new and confirm password match
                    if (String.valueOf(txtnew.getPassword()).equals(String.valueOf(txtconfirm.getPassword()))) {
                        // Change the password
                        DBConnection.ps = con.prepareStatement("UPDATE user SET password = ? WHERE userName = ?");
                        DBConnection.ps.setString(1, String.valueOf(txtnew.getPassword()));
                        DBConnection.ps.setString(2, jLabel7.getText());
                        DBConnection.ps.executeUpdate();
                        JOptionPane.showMessageDialog(null, "Password updated successfully", null, INFORMATION_MESSAGE);
                        clrFields(jPanel21);
                    } else {
                        JOptionPane.showMessageDialog(null, "Passwords do not match", null, ERROR_MESSAGE);
                    }
                        } else {
                            JOptionPane.showMessageDialog(null, "The password should be at least 4 characters long", null, ERROR_MESSAGE);
                } 
                } else {
                    JOptionPane.showMessageDialog(null, "Incorrect password", null, ERROR_MESSAGE);
                }
                con.commit();
            } catch (SQLException e) {
                System.out.println(e);
            } finally {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }//GEN-LAST:event_btnUpdatePass1ActionPerformed

    private void jCheckBox5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox5ActionPerformed
        // TODO add your handling code here:
        if (jCheckBox5.isSelected()){
        txtcurr.setEchoChar((char)0);
        txtnew.setEchoChar((char)0);
        txtconfirm.setEchoChar((char)0);
        }
        else {
        txtcurr.setEchoChar('\u25CF');
        txtnew.setEchoChar('\u25CF');
        txtconfirm.setEchoChar('\u25CF');
        }
    }//GEN-LAST:event_jCheckBox5ActionPerformed

    private void btnChngPassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChngPassActionPerformed
        // TODO add your handling code here:
        TPanel(jPanel20, ChangePassword);
    }//GEN-LAST:event_btnChngPassActionPerformed

    private void btnMngUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMngUserActionPerformed
        // TODO add your handling code here:
        TPanel(jPanel20, MngUsers);
    }//GEN-LAST:event_btnMngUserActionPerformed

    private void btnBackupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackupActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnBackupActionPerformed

    private void btnRestoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRestoreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnRestoreActionPerformed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        // TODO add your handling code here:
        login.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void btnSave7MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSave7MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSave7MouseEntered

    private void btnSave7MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSave7MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSave7MouseExited

    private void btnSave7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSave7ActionPerformed
        // TODO add your handling code here:
        if (txtName1.getText() == null) {
            JOptionPane.showMessageDialog(null, "Some fields are empty", null, ERROR_MESSAGE);
        } else {
        Connection con = DBConnection.DBConnect();
        try {
            DBConnection.ps = con.prepareStatement("INSERT INTO user (userName, userType) VALUES (?, ?)");
            DBConnection.ps.setString(1, txtName1.getText());
            DBConnection.ps.setString(2, jComboBox1.getSelectedItem().toString());
            DBConnection.ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Record saved successfully", null, INFORMATION_MESSAGE);
             table.FilterTable(tblCustomer1, user, jTextField13);
             clrFields(jPanel14);
        } catch (SQLException ex) {
            System.out.println(ex);
            JOptionPane.showMessageDialog(null, "Record could not be saved", null, ERROR_MESSAGE);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        }
    }//GEN-LAST:event_btnSave7ActionPerformed

    private void btnDel5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDel5MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnDel5MouseEntered

    private void btnDel5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDel5MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnDel5MouseExited

    private void btnDel5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDel5ActionPerformed
        // TODO add your handling code here:
        model = (DefaultTableModel) tblCustomer1.getModel();
        int selectedRow = tblCustomer1.getSelectedRow();
        
        op.delete("DELETE FROM user WHERE user_id = ?", (long) model.getValueAt(selectedRow, 0));
        JOptionPane.showMessageDialog(null, "Record deleted successfully", null, INFORMATION_MESSAGE);
        table.FilterTable(tblCustomer1, user, jTextField13);
        clrFields(jPanel14);
        disableOnStart();
        reEnable();
    }//GEN-LAST:event_btnDel5ActionPerformed

    private void tblCustomer1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblCustomer1MouseClicked
        // TODO add your handling code here:
        model = (DefaultTableModel) tblCustomer1.getModel();
            int selectedRow = tblCustomer1.getSelectedRow();
            
            txtName1.setText((String) model.getValueAt(selectedRow, 1));
            jComboBox1.setSelectedItem(model.getValueAt(selectedRow, 2));
            btnDel5.setEnabled(true);
            btnSave7.setEnabled(false);
    }//GEN-LAST:event_tblCustomer1MouseClicked

    private void jTextField13CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField13CaretUpdate
        // TODO add your handling code here:
        table.FilterTable(tblCustomer1, user, jTextField13);
    }//GEN-LAST:event_jTextField13CaretUpdate

    private void btnUpdate2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdate2ActionPerformed
        // TODO add your handling code here:
        Connection con = DBConnection.DBConnect();
        try {
            model = (DefaultTableModel) tblManageStock.getModel();
            int selectedRow = tblManageStock.getSelectedRow();
            
//            if (txtQty.getText().equals("") || txtPrice.getValue() == "" || txtPrice1.getText().equals("")) {
              if (txtUnit1.getText().equals("") || txtItem.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "Some fields are empty", null, INFORMATION_MESSAGE);
            } else {
                DBConnection.ps = con.prepareStatement("UPDATE stock SET qty = ?, price = ?, lowCount = ?, unit = ? WHERE stockid = ?");
                int old = (int) model.getValueAt(selectedRow, 3);
                int newQty = (old + (txtQty.getValue()));
        DBConnection.ps.setInt(1, newQty);
        DBConnection.ps.setInt(2, txtPrice.getValue());
        DBConnection.ps.setInt(3, txtPrice1.getValue());
        DBConnection.ps.setString(4, txtUnit1.getText());
        DBConnection.ps.setLong(5, (long) model.getValueAt(selectedRow, 0));
        DBConnection.ps.executeUpdate();
        JOptionPane.showMessageDialog(null, "Record updated successfully", null, INFORMATION_MESSAGE);
        table1.FilterTable(tblLow, lowQty);
        table.FilterTable(tblStock, stock, txtSearch3);
        table.FilterTable(tblManageStock, stock, jTextField16);
        table1.FilterTable(tblDelivery, delivery);
        clrFields(jPanel11);
        reEnable();
            }
        } catch (HeadlessException | NumberFormatException | SQLException e) {
            System.out.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
            }
        }
    }//GEN-LAST:event_btnUpdate2ActionPerformed

    private void btnDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelActionPerformed
        // TODO add your handling code here:
           // Delete the selected row in the table
                model = (DefaultTableModel) tblDelivery.getModel();
                int SelectedRowIndex = tblDelivery.getSelectedRow();
//                model.removeRow(SelectedRowIndex);
                op.delete("DELETE FROM delivery WHERE deliveryid = ?", (long)model.getValueAt(SelectedRowIndex, 0));
                table1.FilterTable(tblDelivery, delivery);
                clrFields(jPanel6);
                reEnable();
    }//GEN-LAST:event_btnDelActionPerformed

    private void btnCancel3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancel3MouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnCancel3);
    }//GEN-LAST:event_btnCancel3MouseEntered

    private void btnCancel3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancel3MouseExited
        // TODO add your handling code here:
        btnColor(btnCancel3);
    }//GEN-LAST:event_btnCancel3MouseExited

    private void btnCancel3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancel3ActionPerformed
        // TODO add your handling code here:
        clrFields(jPanel11);
        tblManageStock.clearSelection();
        reEnable();
        disableOnStart();
    }//GEN-LAST:event_btnCancel3ActionPerformed

    private void btnSave5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSave5MouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnSave5);
    }//GEN-LAST:event_btnSave5MouseEntered

    private void btnSave5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSave5MouseExited
        // TODO add your handling code here:
        btnColor(btnSave5);
    }//GEN-LAST:event_btnSave5MouseExited

    private void btnClr1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnClr1MouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnClr1);
    }//GEN-LAST:event_btnClr1MouseEntered

    private void btnClr1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnClr1MouseExited
        // TODO add your handling code here:
        btnColor(btnClr1);
    }//GEN-LAST:event_btnClr1MouseExited

    private void btnClr1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClr1ActionPerformed
        // TODO add your handling code here:
        clrFields(jPanel6);
        tblDelivery.clearSelection();
        reEnable();
        disableOnStart();
    }//GEN-LAST:event_btnClr1ActionPerformed

    private void btnSave5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSave5ActionPerformed
        // TODO add your handling code here:
        Connection con = DBConnection.DBConnect();
        if (txtItem1.getText().equals("") || txtUnt.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Some fields are empty", null, INFORMATION_MESSAGE);
        } else {
            try {
            DBConnection.ps = con.prepareStatement("INSERT INTO delivery (item, unit, qty, lowCount, price) VALUES (?, ?, ?, ?, ?)");
            DBConnection.ps.setString(1, txtItem1.getText());
            DBConnection.ps.setString(2, txtUnt.getText());
            DBConnection.ps.setInt(3, txtQty2.getValue());
            DBConnection.ps.setInt(4, txtLowCnt.getValue());
            DBConnection.ps.setInt(5, txtPrice2.getValue());
            DBConnection.ps.executeUpdate();
            table1.FilterTable(tblDelivery, delivery);
            clrFields(jPanel6);
            reEnable();
            } catch (SQLException e) {
                System.out.println(e);
            }
        }
    }//GEN-LAST:event_btnSave5ActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        // TODO add your handling code here:
        Connection con = DBConnection.DBConnect();
        if (txtItem1.getText().equals("") || txtUnt.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Some fields are empty", null, INFORMATION_MESSAGE);
        } else {
            model = (DefaultTableModel) tblDelivery.getModel();
            int selectedRow = tblDelivery.getSelectedRow();
            
            try {
                DBConnection.ps = con.prepareStatement("UPDATE delivery SET item = ?, unit = ?, qty = ?, lowCount = ?, price = ? WHERE deliveryid = ?");
                DBConnection.ps.setString(1, (String) txtItem1.getText());
                DBConnection.ps.setString(2, txtUnt.getText());
                DBConnection.ps.setInt(3, txtQty2.getValue());
                DBConnection.ps.setInt(4, txtLowCnt.getValue());
                DBConnection.ps.setInt(5, txtPrice2.getValue());
                DBConnection.ps.setLong(6, (long) model.getValueAt(selectedRow, 0));
                DBConnection.ps.executeUpdate();
                table1.FilterTable(tblDelivery, delivery);
                tblDelivery.clearSelection();
                clrFields(jPanel6);
                reEnable();
                disableOnStart();
            } catch (SQLException e) {
                System.out.println(e);
            }
        }
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnSave2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSave2ActionPerformed
        // TODO add your handling code here:
        Connection con = DBConnection.DBConnect();
        model = (DefaultTableModel) tblDelivery.getModel();
        try {
            con.setAutoCommit(false);
            DBConnection.ps = con.prepareStatement("INSERT INTO stock (item, unit, qty, lowCount, price) VALUES (?, ?, ?, ?, ?)");
            int rows = tblDelivery.getRowCount();
            for (int i = 0; i < rows; i++) {
                DBConnection.ps.setString(1, (String) model.getValueAt(i, 1));
                DBConnection.ps.setString(2, (String) model.getValueAt(i, 2));
                DBConnection.ps.setInt(3, (int) model.getValueAt(i, 3));
                DBConnection.ps.setInt(4, (int) model.getValueAt(i, 4));
                DBConnection.ps.setInt(5, Integer.parseInt(model.getValueAt(i, 5).toString()));
                
                DBConnection.ps.addBatch();
            }
            DBConnection.ps.executeBatch();
            JOptionPane.showMessageDialog(null, "The items have been added to stock", null, INFORMATION_MESSAGE);
            DBConnection.ps = con.prepareStatement("TRUNCATE TABLE delivery");
            DBConnection.ps.executeUpdate();
            con.commit();
        table1.FilterTable(tblLow, lowQty);
        table.FilterTable(tblStock, stock, txtSearch3);
        table.FilterTable(tblManageStock, stock, jTextField16);
        table1.FilterTable(tblDelivery, delivery);
            clrFields(jPanel6);
            reEnable();
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
            }
        }
    }//GEN-LAST:event_btnSave2ActionPerformed

    private void jTextField16CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTextField16CaretUpdate
        // TODO add your handling code here:
        table.FilterTable(tblManageStock, stock, jTextField16);
    }//GEN-LAST:event_jTextField16CaretUpdate

    private void btnCancel5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancel5MouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnCancel5);
    }//GEN-LAST:event_btnCancel5MouseEntered

    private void btnCancel5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancel5MouseExited
        // TODO add your handling code here:
        btnColor(btnCancel5);
    }//GEN-LAST:event_btnCancel5MouseExited

    private void btnCancel5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancel5ActionPerformed
        // TODO add your handling code here:
        clrFields(jPanel4);
        tblOrder.clearSelection();
        reEnable();
        disableOnStart();
    }//GEN-LAST:event_btnCancel5ActionPerformed

    private void btnDel1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDel1MouseExited
        // TODO add your handling code here:
        btnDel1.setBackground(new Color(200, 0, 0));
    }//GEN-LAST:event_btnDel1MouseExited

    private void btnDel1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDel1MouseEntered
        // TODO add your handling code here:
        btnDel1.setBackground(new Color(241, 94, 94));
    }//GEN-LAST:event_btnDel1MouseEntered

    private void btnSave3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSave3MouseExited
        // TODO add your handling code here:
        btnColor(btnSave3);
    }//GEN-LAST:event_btnSave3MouseExited

    private void btnSave3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSave3MouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnSave3);
    }//GEN-LAST:event_btnSave3MouseEntered

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        try {
            model = (DefaultTableModel) jTable1.getModel();
        int selectedRow = jTable1.getSelectedRow();

        txtItem2.setText((String) model.getValueAt(selectedRow, 1));
        txtQty1.setValue(Integer.parseInt((String) model.getValueAt(selectedRow, 2)));
        txtSubUnit.setValue(Integer.parseInt((String) model.getValueAt(selectedRow, 3)));
        btnDel1.setEnabled(true);
        } catch (NumberFormatException e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_jTable1MouseClicked

    private void btnCancel4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancel4MouseEntered
        // TODO add your handling code here:
        btnHoverColor(btnCancel4);
    }//GEN-LAST:event_btnCancel4MouseEntered

    private void btnCancel4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancel4MouseExited
        // TODO add your handling code here:
        btnColor(btnCancel4);
    }//GEN-LAST:event_btnCancel4MouseExited

    private void btnCancel4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancel4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnCancel4ActionPerformed

    private void tblSale1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblSale1MouseClicked
        // TODO add your handling code here:
        tblSale.clearSelection();
        Connection con = DBConnection.DBConnect();
        Boolean exists = false;
        Object rowEntry;
        int rows = 0;
        try {
            Object[] row = new Object[5];
            model = (DefaultTableModel) tblSale.getModel();
            DefaultTableModel model1 = (DefaultTableModel) tblSale1.getModel();
            int selectedRow = tblSale1.getSelectedRow();
            
            int rowCount = tblSale.getRowCount();
            for (int i = 0; i < rowCount; i++) {
            rowEntry = model.getValueAt(i, 0);
                if (rowEntry.equals(model1.getValueAt(selectedRow, 0))) {
                    exists = true;
                      rows = i;
            } 
            }
                if (exists == true) {
                    int newQty = Integer.parseInt(model.getValueAt(rows, 2).toString()) + 1;
                    model.setValueAt(newQty, rows, 2);
                } else {
                     row[0] = model1.getValueAt(selectedRow, 0);
                    row[1] = (model1.getValueAt(selectedRow, 1) + " " + model1.getValueAt(selectedRow, 2));
                    row[2] = 1;

                    DBConnection.ps = con.prepareStatement("SELECT * FROM stock WHERE stockid = ?");
                    DBConnection.ps.setLong(1, (long) model1.getValueAt(selectedRow, 0));
                    DBConnection.rs = DBConnection.ps.executeQuery();
                    while (DBConnection.rs.next()) {
                    row[3] = DBConnection.rs.getInt(6);
                    }

                    int amnt = (Integer.parseInt(row[2].toString()) * Integer.parseInt(row[3].toString()));
                    row[4] = amnt;

                    model.addRow(row);
                }
        } catch (NumberFormatException | SQLException e) {
            System.out.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ManageDatabase.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_tblSale1MouseClicked

    private void btnProcessActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcessActionPerformed
        // TODO add your handling code here:
        jLabel16.setText(txtTotal.getText());
        SalePayment.setLocationRelativeTo(null);
        SalePayment.setVisible(true);
    }//GEN-LAST:event_btnProcessActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        SalePayment.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void txtSearch2CaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txtSearch2CaretUpdate
        // TODO add your handling code here:
        table.FilterTable(tblSale1, itemSearch, txtSearch2);
    }//GEN-LAST:event_txtSearch2CaretUpdate

    private void tblSaleCaretPositionChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_tblSaleCaretPositionChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_tblSaleCaretPositionChanged

    private void tblSale1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tblSale1FocusLost
        // TODO add your handling code here:
        tblSale1.clearSelection();
    }//GEN-LAST:event_tblSale1FocusLost

    private void tblSalePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_tblSalePropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_tblSalePropertyChange

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                   if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Body;
    private javax.swing.JPanel CategoryTab;
    private javax.swing.JPanel ChangePassword;
    private javax.swing.JPanel Contacts;
    private javax.swing.JPanel DeliveryReport;
    private javax.swing.JPanel DeliveryTab;
    private javax.swing.JPanel Header;
    private javax.swing.JPanel Inventory;
    private javax.swing.JPanel Invoice;
    private javax.swing.JPanel InvoiceReport;
    private javax.swing.JPanel LowStock;
    private javax.swing.JPanel ManageItems;
    private javax.swing.JPanel Menu;
    private javax.swing.JPanel MngUsers;
    private javax.swing.JPanel NewSale;
    private javax.swing.JPanel PaymentReport;
    private javax.swing.JPanel Payments;
    private javax.swing.JPanel Reports;
    private javax.swing.JTabbedPane ReportsTab;
    private javax.swing.JDialog SalePayment;
    private javax.swing.JPanel SalesReport;
    private javax.swing.JPanel Setting;
    private javax.swing.JPanel StockTab;
    private javax.swing.JButton btnBackup;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnCancel1;
    private javax.swing.JButton btnCancel2;
    private javax.swing.JButton btnCancel3;
    private javax.swing.JButton btnCancel4;
    private javax.swing.JButton btnCancel5;
    private javax.swing.JButton btnChngPass;
    private javax.swing.JButton btnClr1;
    private javax.swing.JButton btnContacts;
    private javax.swing.JButton btnDel;
    private javax.swing.JButton btnDel1;
    private javax.swing.JButton btnDel2;
    private javax.swing.JButton btnDel3;
    private javax.swing.JButton btnDel4;
    private javax.swing.JButton btnDel5;
    private javax.swing.JButton btnInvoice;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnMngUser;
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnNew1;
    private javax.swing.JButton btnNew2;
    private javax.swing.JButton btnPay;
    private javax.swing.JButton btnPayments;
    private javax.swing.JButton btnPending;
    private javax.swing.JButton btnPending1;
    private javax.swing.JButton btnProcess;
    private javax.swing.JButton btnReports;
    private javax.swing.JButton btnRestore;
    private javax.swing.JButton btnSale;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSave1;
    private javax.swing.JButton btnSave2;
    private javax.swing.JButton btnSave3;
    private javax.swing.JButton btnSave4;
    private javax.swing.JButton btnSave5;
    private javax.swing.JButton btnSave6;
    private javax.swing.JButton btnSave7;
    private javax.swing.JButton btnSettings;
    private javax.swing.JButton btnStock;
    private javax.swing.JButton btnStockReport;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JButton btnUpdate1;
    private javax.swing.JButton btnUpdate2;
    private javax.swing.JButton btnUpdatePass1;
    private javax.swing.JButton btnVoid;
    private javax.swing.JButton btnVoid1;
    private javax.swing.JButton btnVoid2;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JCheckBox jCheckBox5;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField12;
    private javax.swing.JTextField jTextField13;
    private javax.swing.JTextField jTextField16;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JLabel lblSaleNo;
    private javax.swing.JLabel lblSaleNo1;
    private javax.swing.JLabel lblSaleNo2;
    private javax.swing.JTable tblCustomer;
    private javax.swing.JTable tblCustomer1;
    private javax.swing.JTable tblDelivery;
    private javax.swing.JTable tblInvoice;
    private javax.swing.JTable tblLow;
    private javax.swing.JTable tblManageStock;
    private javax.swing.JTable tblOrder;
    private javax.swing.JTable tblPayment;
    private javax.swing.JTable tblSale;
    private javax.swing.JTable tblSale1;
    private javax.swing.JTable tblStock;
    private javax.swing.JTextField txtCustomer;
    private javax.swing.JTextArea txtDes;
    private javax.swing.JTextField txtItem;
    private javax.swing.JTextField txtItem1;
    private javax.swing.JTextField txtItem2;
    private javax.swing.JTextField txtItem3;
    private javax.swing.JTextField txtItem4;
    private com.toedter.components.JSpinField txtLowCnt;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtName1;
    private javax.swing.JTextField txtPhone;
    private com.toedter.components.JSpinField txtPrice;
    private com.toedter.components.JSpinField txtPrice1;
    private com.toedter.components.JSpinField txtPrice2;
    private com.toedter.components.JSpinField txtQty;
    private com.toedter.components.JSpinField txtQty1;
    private com.toedter.components.JSpinField txtQty2;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtSearch1;
    private javax.swing.JTextField txtSearch2;
    private javax.swing.JTextField txtSearch3;
    private com.toedter.components.JSpinField txtSubUnit;
    private com.toedter.components.JSpinField txtSubUnit1;
    private javax.swing.JTextField txtSupplier;
    private javax.swing.JTextField txtSupplier1;
    private javax.swing.JLabel txtTotal;
    private javax.swing.JLabel txtTotal1;
    private javax.swing.JLabel txtTotal2;
    private javax.swing.JTextField txtUnit1;
    private javax.swing.JTextField txtUnt;
    private javax.swing.JPasswordField txtconfirm;
    private javax.swing.JPasswordField txtcurr;
    private javax.swing.JPasswordField txtnew;
    // End of variables declaration//GEN-END:variables

    public void saleTableListener (){
    tblSale.getModel().addTableModelListener((TableModelEvent e) -> {
        model = (DefaultTableModel) tblSale.getModel();
        Object[] value = new Object[2];
        
        // Multiply the colummns
        try {
            for (int i = 0; i < tblSale.getRowCount(); i++) {
            value[0] = model.getValueAt(i, 2);
            value[1] = model.getValueAt(i, 3);
            int amounts = Integer.parseInt(value[0].toString()) * Integer.parseInt(value[1].toString());
//            model.setValueAt(value[2], i, 4);
//            // Add the amounts
            total = total + amounts;
        }
            
//            for (int i = 0; i <= amount.size(); i++) {
//                tblSale.setValueAt(amount.get(i), i, 4);
//            }
        // Display total in the label
        txtTotal.setText(String.valueOf(total));
        } catch (NumberFormatException ex) {
            System.out.println(ex);
        }
    });
}
}
