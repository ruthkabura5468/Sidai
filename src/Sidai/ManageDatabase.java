
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package Sidai;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.SQLException;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ruth
 */
public class ManageDatabase {
    public void Tables() {
        Connection con = DBConnection.DBConnect();

        try {
            con.setAutoCommit(false);

            // Create tables if it doesn't exist
            // user table
            DBConnection.ps = con.prepareStatement("CREATE TABLE IF NOT EXISTS user "
                    + "(user_id IDENTITY PRIMARY KEY," 
                    + "userName VARCHAR(500) UNIQUE,"
                    + "password VARCHAR(500) DEFAULT '1234'," 
                    + "userType VARCHAR(500))");
            DBConnection.ps.executeUpdate();
            System.out.println("Table user created");

            // Insert super admin if user table is empty
            DBConnection.ps = con.prepareStatement("SELECT * FROM user");
            DBConnection.rs = DBConnection.ps.executeQuery();

            if (DBConnection.rs.next() == false) {
                DBConnection.ps = con.prepareStatement(
                    "INSERT INTO user (userName, password, userType) VALUES ('superadmin', 'P@s5w0rd!', 'Super')");
                DBConnection.ps.executeUpdate();
                System.out.println("Superadmin record inserted");
            }
            
             // contact table
                DBConnection.ps = con.prepareStatement("CREATE TABLE IF NOT EXISTS contact ("
                        + "customerid IDENTITY PRIMARY KEY," 
                        + "name VARCHAR_IGNORECASE(500)," 
                        + "phone VARCHAR(500));");
                DBConnection.ps.executeUpdate();
                System.out.println("Table contact created");

                // order table
                DBConnection.ps = con.prepareStatement("CREATE TABLE IF NOT EXISTS orders ("
                        + "orderid IDENTITY PRIMARY KEY," 
                        + "item VARCHAR_IGNORECASE(500)," 
                        + "quantity INT,"
                        + "description VARCHAR_IGNORECASE(500));");
                DBConnection.ps.executeUpdate();
                System.out.println("Table orders created");

                // supplier table
                DBConnection.ps = con.prepareStatement("CREATE TABLE IF NOT EXISTS supplier ("
                        + "supplierid IDENTITY PRIMARY KEY," 
                        + "name VARCHAR_IGNORECASE(500),"
                        + "phone VARCHAR(500),"
                        + "item VARCHAR_IGNORECASE(500));");
                DBConnection.ps.executeUpdate();
                System.out.println("Table supplier created");
                
                // stock table
                DBConnection.ps = con.prepareStatement("CREATE TABLE IF NOT EXISTS stock ("
                        + "stockid IDENTITY PRIMARY KEY," 
                        + "item VARCHAR_IGNORECASE(500),"
                        + "unit VARCHAR_IGNORECASE(500),"
                        + "qty INT,"
                        + "lowCount INT,"
                        + "price INT);");
                DBConnection.ps.executeUpdate();
                System.out.println("Table stock created");
                
                // subDivision table
                DBConnection.ps = con.prepareStatement("CREATE TABLE IF NOT EXISTS subDivision ("
                        + "divisionid IDENTITY PRIMARY KEY," 
                        + "item VARCHAR_IGNORECASE(500),"
                        + "itemQty INT,"
                        + "subUnit VARCHAR_IGNORECASE(500),"
                        + "subQty INT,"
                        + "price INT);");
                DBConnection.ps.executeUpdate();
                System.out.println("Table subDivision created");
                
                // delivery table
                DBConnection.ps = con.prepareStatement("CREATE TABLE IF NOT EXISTS delivery ("
                        + "deliveryid IDENTITY PRIMARY KEY,"
                        + "item VARCHAR_IGNORECASE(500),"
                        + "unit VARCHAR_IGNORECASE(500),"
                        + "qty INT,"
                        + "lowCount INT,"
                        + "price INT);");
                DBConnection.ps.executeUpdate();
                System.out.println("Table subDivision created");
                
                // Sales table
                DBConnection.ps = con.prepareStatement("CREATE TABLE IF NOT EXISTS sales ("
                        + "saleid IDENTITY PRIMARY KEY,"
                        + "saleno INT,"
                        + "saledate DATE,"
                        + "customer VARCHAR_IGNORECASE(500),"
                        + "itemid BIGINT,"
                        + "item INT,"
                        + "qty INT,"
                        + "amount INT,"
                        + "total INT,"
                        + "status BOOLEAN);");
                DBConnection.ps.executeUpdate();
                System.out.println("Table sales created");
                con.commit();
        } catch (SQLException ex) {
            Logger.getLogger(ManageDatabase.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ManageDatabase.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}

