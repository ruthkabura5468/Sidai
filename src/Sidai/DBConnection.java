
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package Sidai;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

/**
 *
 * @author Ruth
 */
public class DBConnection {
    public static PreparedStatement ps;
    public static ResultSet rs;
    public static Statement stmt;

    public static Connection DBConnect() {
        Connection con;

        try {
            Class.forName("org.h2.Driver");
            con = DriverManager.getConnection("jdbc:h2:~/Sidai", "", "");

            return con;
        } catch (ClassNotFoundException | SQLException ex) {
            JOptionPane.showMessageDialog(null, "Ensure the server is not already in use");
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);

            return null;
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
