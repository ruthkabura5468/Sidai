
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package Sidai;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Ruth
 */
public class FillTable1 {
    public void FilterTable(JTable table, String Query) {
        Connection con = DBConnection.DBConnect();

        try {
            DBConnection.ps = con.prepareStatement(Query);
            DBConnection.rs = DBConnection.ps.executeQuery();

            // To remove previously added rows
            while (table.getRowCount() > 0) {
                ((DefaultTableModel) table.getModel()).removeRow(0);
            }

            int columns = DBConnection.rs.getMetaData().getColumnCount();

            while (DBConnection.rs.next()) {
                Object[] row = new Object[columns];

                for (int i = 1; i <= columns; i++) {
                    row[i - 1] = DBConnection.rs.getObject(i);
                }

                ((DefaultTableModel) table.getModel()).insertRow(DBConnection.rs.getRow() - 1, row);
            }
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {}
        }
    }
}


